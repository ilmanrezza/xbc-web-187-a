package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_AUDIT_LOG")
public class AuditModel {

	private Integer id;
	private String type;
	private String jsonInsert;
	private String jsonBefore;
	private String jsonAfter;
	private String jsonDelete;

	// Audit
	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	@Id
	@Column(name = "ID")
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TYPE")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "JSON_INSERT")
	public String getJsonInsert() {
		return jsonInsert;
	}

	public void setJsonInsert(String jsonInsert) {
		this.jsonInsert = jsonInsert;
	}

	@Column(name = "JSON_BEFORE")
	public String getJsonBefore() {
		return jsonBefore;
	}

	public void setJsonBefore(String jsonBefore) {
		this.jsonBefore = jsonBefore;
	}

	@Column(name = "JSON_AFTER")
	public String getJsonAfter() {
		return jsonAfter;
	}

	public void setJsonAfter(String jsonAfter) {
		this.jsonAfter = jsonAfter;
	}

	@Column(name = "JSON_DELETE")
	public String getJsonDelete() {
		return jsonDelete;
	}

	public void setJsonDelete(String jsonDelete) {
		this.jsonDelete = jsonDelete;
	}

	// -----------------------------------------------------------------//
	@Column(name = "CREATED_BY", length = 11)
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}

	// -----------------------------------------------------------------//

	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
