package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_USER")
public class UserModel {

	private Integer idUser;
	private String username;
	private String password;
	private String email;

	// join ke t_role
	private Integer idRole;
	private RoleModel roleModel;

	private Boolean mobileFlag;
	private Integer mobileToken;

	private Integer idCreatedBy;
	private Date createdOn;

	private Integer idModifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private Date deletedOn;

	private Boolean isDelete;

	// untuk security
	private Boolean enabled;

	@Id
	@Column(name = "ID_USER", nullable = false, length = 11)
	@GeneratedValue
	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	@Column(name = "USERNAME", nullable = false, length = 50)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "PASSWORD", nullable = false, length = 50)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "EMAIL", nullable = false, length = 100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// -----------------------------------------------------------------//
	@Column(name = "ID_ROLE", length = 11)
	public Integer getIdRole() {
		return idRole;
	}

	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}

	@ManyToOne
	@JoinColumn(name = "ID_ROLE", nullable = false, updatable = false, insertable = false)
	public RoleModel getRoleModel() {
		return roleModel;
	}

	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	// -----------------------------------------------------------------//

	@Column(name = "MOBILE_FLAG", nullable = false)
	public Boolean getMobileFlag() {
		return mobileFlag;
	}

	public void setMobileFlag(Boolean mobileFlag) {
		this.mobileFlag = mobileFlag;
	}

	@Column(name = "MOBILE_TOKEN")
	public Integer getMobileToken() {
		return mobileToken;
	}

	public void setMobileToken(Integer mobileToken) {
		this.mobileToken = mobileToken;
	}

	@Column(name = "CREATED_BY", nullable = false)
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@Column(name = "CREATED_ON", nullable = false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "DELETED_BY")
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getisDelete() {
		return isDelete;
	}

	public void setisDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Column(name = "ENABLED")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

}
