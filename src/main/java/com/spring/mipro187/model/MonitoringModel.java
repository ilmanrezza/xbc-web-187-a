package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_MONITORING")
public class MonitoringModel {

	private Integer idMonitoring;

	// Join ke t_biodata
	private Integer idBiodata;
	private BiodataModel biodataModel;

	private Date idleDate;
	private String lastProject;
	private String idleReason;
	private Date placementDate;
	private String placementAt;
	private String notes;

	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	private Integer idModifiedBy;
	private UserModel modifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private UserModel deletedBy;
	private Date deletedOn;

	private Boolean isDelete;

	@Id
	@Column(name = "ID_MONITORING", nullable = false)
	@GeneratedValue
	public Integer getIdMonitoring() {
		return idMonitoring;
	}

	public void setIdMonitoring(Integer idMonitoring) {
		this.idMonitoring = idMonitoring;
	}

	// -----------------------------------------------------------------//
	@Column(name = "ID_BIODATA", nullable = false, length = 11)
	public Integer getIdBiodata() {
		return idBiodata;
	}

	public void setIdBiodata(Integer idBiodata) {
		this.idBiodata = idBiodata;
	}

	@ManyToOne
	@JoinColumn(name = "ID_BIODATA", nullable = true, updatable = false, insertable = false)
	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}
	// -----------------------------------------------------------------//

	@Column(name = "IDLE_DATE", nullable = false)
	public Date getIdleDate() {
		return idleDate;
	}

	public void setIdleDate(Date idleDate) {
		this.idleDate = idleDate;
	}

	@Column(name = "LAST_PROJECT", length = 50)
	public String getLastProject() {
		return lastProject;
	}

	public void setLastProject(String lastProject) {
		this.lastProject = lastProject;
	}

	@Column(name = "IDLE_REASON", length = 255)
	public String getIdleReason() {
		return idleReason;
	}

	public void setIdleReason(String idleReason) {
		this.idleReason = idleReason;
	}

	@Column(name = "PLACEMENT_DATE")
	public Date getPlacementDate() {
		return placementDate;
	}

	public void setPlacementDate(Date placementDate) {
		this.placementDate = placementDate;
	}

	@Column(name = "PLACEMENT_AT", length = 50)
	public String getPlacementAt() {
		return placementAt;
	}

	public void setPlacementAt(String placementAt) {
		this.placementAt = placementAt;
	}

	@Column(name = "NOTES", length = 255)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	// -----------------------------------------------------------------//
	@Column(name = "CREATED_BY")
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}
	// -----------------------------------------------------------------//
	
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "MODIFIED_BY")
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	// -----------------------------------------------------------------//
	
	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "DELETED_BY")
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}
	// -----------------------------------------------------------------//
	
	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
