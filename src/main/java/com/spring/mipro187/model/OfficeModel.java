package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_OFFICE")
public class OfficeModel {

	private Integer idOffice;
	private String nameOffice;
	private String phoneOffice;
	private String email;
	private String address;
	private String notesOffice;

	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	private Integer idModifiedBy;
	private UserModel modifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private UserModel deletedBy;
	private Date deletedOn;

	private Boolean isDelete;

	@Id
	@Column(name = "ID_OFFICE", nullable = false, length = 11)
	@GeneratedValue
	public Integer getIdOffice() {
		return idOffice;
	}

	public void setIdOffice(Integer idOffice) {
		this.idOffice = idOffice;
	}

	@Column(name = "NAME_OFFICE", nullable = false, length = 50)
	public String getNameOffice() {
		return nameOffice;
	}

	public void setNameOffice(String nameOffice) {
		this.nameOffice = nameOffice;
	}

	@Column(name = "PHONE_OFFICE", length = 50)
	public String getPhoneOffice() {
		return phoneOffice;
	}

	public void setPhoneOffice(String phoneOffice) {
		this.phoneOffice = phoneOffice;
	}

	@Column(name = "EMAIL", length = 255)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "ADDRESS", length = 500)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "NOTES_OFFICE", length = 500)
	public String getNotesOffice() {
		return notesOffice;
	}

	public void setNotesOffice(String notesOffice) {
		this.notesOffice = notesOffice;
	}

	// -----------------------------------------------------------------//
	@Column(name = "CREATED_BY", length = 11)
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "MODIFIED_BY", length = 11)
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "DELETED_BY", length = 11)
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
