package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_ASSIGNMENT")
public class AssignmentModel {

	private Integer idAssignment;

	// Join ke t_biodata
	private Integer idBiodata;
	private BiodataModel biodataModel;

	private String title;
	private Date startDate;
	private Date endDate;
	private String description;
	private Date realizationDate;
	private String notes;
	private Boolean isHold;
	private Boolean isDone;

	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	private Integer idModifiedBy;
	private UserModel modifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private UserModel deletedBy;
	private Date deletedOn;

	private Boolean isDelete;

	@Id
	@Column(name = "ID_ASSIGNMENT")
	@GeneratedValue
	public Integer getIdAssignment() {
		return idAssignment;
	}

	public void setIdAssignment(Integer idAssignment) {
		this.idAssignment = idAssignment;
	}

	// -----------------------------------------------------------------//
	@Column(name = "ID_BIODATA", nullable = false)
	public Integer getIdBiodata() {
		return idBiodata;
	}

	public void setIdBiodata(Integer idBiodata) {
		this.idBiodata = idBiodata;
	}

	@ManyToOne
	@JoinColumn(name = "ID_BIODATA", nullable = true, updatable = false, insertable = false)
	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}
	// -----------------------------------------------------------------//

	@Column(name = "TITLE")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "START_DATE")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "END_DATE")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "REALIZATION_DATE")
	public Date getRealizationDate() {
		return realizationDate;
	}

	public void setRealizationDate(Date realizationDate) {
		this.realizationDate = realizationDate;
	}

	@Column(name = "NOTES")
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name = "IS_HOLD")
	public Boolean getIsHold() {
		return isHold;
	}

	public void setIsHold(Boolean isHold) {
		this.isHold = isHold;
	}

	@Column(name = "IS_DONE")
	public Boolean getIsDone() {
		return isDone;
	}

	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}

	// -----------------------------------------------------------------//
	@Column(name = "CREATED_BY")
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "MODIFIED_BY")
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "DELETED_BY")
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
