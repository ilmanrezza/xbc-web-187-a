package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_TESTTYPE")
public class TestTypeModel {

	private Integer idTestType;
	private String namaTestType;
	private String noteTestType;
	private Integer typeOfAnswer;

	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	private Integer idModifiedBy;
	private UserModel modifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private UserModel deletedBy;
	private Date deletedOn;

	private Boolean isDelete;

	@Id
	@Column(name = "ID_TEST_TYPE", nullable = false, length = 11)
	@GeneratedValue
	public Integer getIdTestType() {
		return idTestType;
	}

	public void setIdTestType(Integer idTestType) {
		this.idTestType = idTestType;
	}

	@Column(name = "TEST_TYPE", nullable = false, length = 50)
	public String getNamaTestType() {
		return namaTestType;
	}

	public void setNamaTestType(String namaTestType) {
		this.namaTestType = namaTestType;
	}

	@Column(name = "NOTES", length = 255)
	public String getNoteTestType() {
		return noteTestType;
	}

	public void setNoteTestType(String noteTestType) {
		this.noteTestType = noteTestType;
	}

	@Column(name = "TYPE_OF_ANSWER", length = 11, nullable = false)
	public Integer getTypeOfAnswer() {
		return typeOfAnswer;
	}

	public void setTypeOfAnswer(Integer typeOfAnswer) {
		this.typeOfAnswer = typeOfAnswer;
	}

	// -----------------------------------------------------------------//
	@Column(name = "CREATED_BY", length = 11)
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = false, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "CREATED_ON", nullable = false)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "MODIFIED_BY", length = 11)
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "DELETED_BY", length = 11)
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
