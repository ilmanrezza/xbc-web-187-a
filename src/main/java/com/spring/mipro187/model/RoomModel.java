package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_ROOM")
public class RoomModel {

	private Integer idRoom;
	private String codeRoom;
	private String nameRoom;
	private Integer capacity;
	private Boolean anyProjector;
	private String notesRoom;
	
	// Join ke t_office
	private Integer idOffice;
	private OfficeModel officeModel;

	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	private Integer idModifiedBy;
	private UserModel modifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private UserModel deletedBy;
	private Date deletedOn;

	private Boolean isDelete;

	@Id
	@Column(name = "ID_ROOM", nullable = false, length = 11)
	@GeneratedValue
	public Integer getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Integer idRoom) {
		this.idRoom = idRoom;
	}

	@Column(name = "CODE_ROOM", nullable = false, length = 50)
	public String getCodeRoom() {
		return codeRoom;
	}

	public void setCodeRoom(String codeRoom) {
		this.codeRoom = codeRoom;
	}

	@Column(name = "NAME_ROOM", nullable = false, length = 50)
	public String getNameRoom() {
		return nameRoom;
	}

	public void setNameRoom(String nameRoom) {
		this.nameRoom = nameRoom;
	}

	@Column(name = "CAPACITY", nullable = false, length = 5)
	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	@Column(name = "PROJECTOR", nullable = false)
	public Boolean getAnyProjector() {
		return anyProjector;
	}

	public void setAnyProjector(Boolean anyProjector) {
		this.anyProjector = anyProjector;
	}

	@Column(name = "NOTES", length = 500)
	public String getNotesRoom() {
		return notesRoom;
	}

	public void setNotesRoom(String notesRoom) {
		this.notesRoom = notesRoom;
	}

	@Column(name = "ID_OFFICE", nullable = false, length = 11)
	public Integer getIdOffice() {
		return idOffice;
	}

	public void setIdOffice(Integer idOffice) {
		this.idOffice = idOffice;
	}

	@ManyToOne
	@JoinColumn(name = "ID_OFFICE", nullable = true, updatable = false, insertable = false)
	public OfficeModel getOfficeModel() {
		return officeModel;
	}

	public void setOfficeModel(OfficeModel officeModel) {
		this.officeModel = officeModel;
	}

	// -----------------------------------------------------------------//
	@Column(name = "CREATED_BY", length = 11)
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "MODIFIED_BY", length = 11)
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// -----------------------------------------------------------------//
	@Column(name = "DELETED_BY", length = 11)
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}
	// -----------------------------------------------------------------//

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Column(name = "IS_DELETE", nullable = false)
	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}
