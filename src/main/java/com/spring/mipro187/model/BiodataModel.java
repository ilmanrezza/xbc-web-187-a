package com.spring.mipro187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "T_BIODATA")
public class BiodataModel {

	private Integer id;
	private String name;
	private String gender;
	private String lastEducation;
	private String graduationYear;
	private String educationalLevel;
	private String majors;
	private String gpa;
	private Integer bootcampTestType;
	private Integer iq;
	private String du;
	private Integer arithmetic;
	private Integer nestedLogic;
	private Integer joinTable;
	private String tro;
	private String notes;
	private String interviewer;

	private boolean isDelete;

	private Integer idCreatedBy;
	private UserModel createdBy; // join user
	private Date createdOn;

	private Integer idModifiedBy;
	private UserModel modifiedBy;
	private Date modifiedOn;

	private Integer idDeletedBy;
	private UserModel deletedBy;
	private Date deletedOn;

	// join table biodata-bootcampTestType

	private BootcamptesModel bootcamptesModel;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "T_BIODATA")
	@TableGenerator(name = "T_BIODATA", table = "M_SEQUENCE", pkColumnName = "SEQUENCE_NAME", pkColumnValue = "ID", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "GENDER")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "LAST_EDUCATION")
	public String getLastEducation() {
		return lastEducation;
	}

	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}

	@Column(name = "GRADUATION_YEAR")
	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	@Column(name = "EDUCATIONAL_LEVEL")
	public String getEducationalLevel() {
		return educationalLevel;
	}

	public void setEducationalLevel(String educationalLevel) {
		this.educationalLevel = educationalLevel;
	}

	@Column(name = "MAJORS")
	public String getMajors() {
		return majors;
	}

	public void setMajors(String majors) {
		this.majors = majors;
	}

	@Column(name = "GPA")
	public String getGpa() {
		return gpa;
	}

	public void setGpa(String gpa) {
		this.gpa = gpa;
	}

	@Column(name = "BOOTCAMP_TEST_TYPE")
	public Integer getBootcampTestType() {
		return bootcampTestType;
	}

	public void setBootcampTestType(Integer bootcampTestType) {
		this.bootcampTestType = bootcampTestType;
	}

	@Column(name = "IQ")
	public Integer getIq() {
		return iq;
	}

	public void setIq(Integer iq) {
		this.iq = iq;
	}

	@Column(name = "DU")
	public String getDu() {
		return du;
	}

	public void setDu(String du) {
		this.du = du;
	}

	@Column(name = "ARITHMETIC")
	public Integer getArithmetic() {
		return arithmetic;
	}

	public void setArithmetic(Integer arithmetic) {
		this.arithmetic = arithmetic;
	}

	@Column(name = "NESTED_LOGIC")
	public Integer getNestedLogic() {
		return nestedLogic;
	}

	public void setNestedLogic(Integer nestedLogic) {
		this.nestedLogic = nestedLogic;
	}

	@Column(name = "JOIN_TABLE")
	public Integer getJoinTable() {
		return joinTable;
	}

	public void setJoinTable(Integer joinTable) {
		this.joinTable = joinTable;
	}

	@Column(name = "TRO")
	public String getTro() {
		return tro;
	}

	public void setTro(String tro) {
		this.tro = tro;
	}

	@Column(name = "NOTES")
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name = "INTERVIEWER")
	public String getInterviewer() {
		return interviewer;
	}

	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}

	@Column(name = "IS_DELETE")
	public boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Column(name = "CREATED_BY")
	public Integer getIdCreatedBy() {
		return idCreatedBy;
	}

	public void setIdCreatedBy(Integer idCreatedBy) {
		this.idCreatedBy = idCreatedBy;
	}

	@ManyToOne
	@JoinColumn(name = "CREATED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserModel createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getIdModifiedBy() {
		return idModifiedBy;
	}

	public void setIdModifiedBy(Integer idModifiedBy) {
		this.idModifiedBy = idModifiedBy;
	}

	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "DELETED_BY")
	public Integer getIdDeletedBy() {
		return idDeletedBy;
	}

	public void setIdDeletedBy(Integer idDeletedBy) {
		this.idDeletedBy = idDeletedBy;
	}

	@ManyToOne
	@JoinColumn(name = "DELETED_BY", nullable = true, updatable = false, insertable = false)
	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Column(name = "DELETED_ON")
	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@ManyToOne
	@JoinColumn(name = "BOOTCAMP_TEST_TYPE", nullable = true, updatable = false, insertable = false)
	public BootcamptesModel getBootcamptesModel() {
		return bootcamptesModel;
	}

	public void setBootcamptesModel(BootcamptesModel bootcamptesModel) {
		this.bootcamptesModel = bootcamptesModel;
	}

}
