package com.spring.mipro187.dao;

import java.util.List;

import com.spring.mipro187.model.TestTypeModel;

public interface TestTypeDao {

	public List<TestTypeModel> searchAll();
	
	public TestTypeModel searchId(String idTestType);
	
	public void create(TestTypeModel testTypeModel);
	
	public void update(TestTypeModel testTypeModel);
	
	public List<TestTypeModel> searchNamaLike(String namaTestType);
}
