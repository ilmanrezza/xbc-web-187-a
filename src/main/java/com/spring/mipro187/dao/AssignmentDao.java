package com.spring.mipro187.dao;

import java.util.List;

import com.spring.mipro187.model.AssignmentModel;

public interface AssignmentDao {

	public List<AssignmentModel> searchAll();

	public void create(AssignmentModel assignmentModel);

	public AssignmentModel searchId(String idAssignment);
	
	public void update(AssignmentModel assignmentModel);
	
	public List<AssignmentModel> searchByDate(String date);
}
