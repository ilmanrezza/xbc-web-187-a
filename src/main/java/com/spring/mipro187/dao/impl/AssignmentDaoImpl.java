package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.AssignmentDao;
import com.spring.mipro187.model.AssignmentModel;

@Repository
public class AssignmentDaoImpl implements AssignmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<AssignmentModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<AssignmentModel> assignmentModelList = new ArrayList<AssignmentModel>();

		String query = " from AssignmentModel ";
		String kondisi = " where isDelete != 1 ";

		assignmentModelList = session.createQuery(query + kondisi).list();

		return assignmentModelList;
	}

	@Override
	public void create(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(assignmentModel);
	}

	@Override
	public AssignmentModel searchId(String idAssignment) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		AssignmentModel assignmentModel = new AssignmentModel();

		String query = " from AssignmentModel ";
		String kondisi = " where idAssignment = '" + idAssignment + "' ";

		assignmentModel = (AssignmentModel) session.createQuery(query + kondisi).uniqueResult();

		return assignmentModel;
	}

	@Override
	public void update(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(assignmentModel);
	}

	@Override
	public List<AssignmentModel> searchByDate(String date) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<AssignmentModel> assignmentModelList = new ArrayList<AssignmentModel>();

		// select * from test where cast ([date] as date) = '03/19/2014';
		String query = " from AssignmentModel ";
		String kondisi = " where (startDate like '%" + date + "%') and isDelete !=1 ";

		assignmentModelList = session.createQuery(query + kondisi).list();

		return assignmentModelList;
	}

}
