package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.TestTypeDao;
import com.spring.mipro187.model.TestTypeModel;

@Repository
public class TestTypeDaoImpl implements TestTypeDao {

	@Autowired
	private SessionFactory sessionFactory;

	// ============================= List ============================= //
	@Override
	public List<TestTypeModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<TestTypeModel> testTypeModelList = new ArrayList<TestTypeModel>();

		String query = " from TestTypeModel ";
		String kondisi = " where isDelete != 1 ";

		testTypeModelList = session.createQuery(query + kondisi).list();

		return testTypeModelList;
	}

	// ============================= Create ============================= //
	@Override
	public void create(TestTypeModel testTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(testTypeModel);
	}

	// ============================= Search Id ============================= // //
	@Override
	public TestTypeModel searchId(String idTestType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		TestTypeModel testTypeModel = new TestTypeModel();

		String query = " from TestTypeModel ";
		String kondisi = " where idTestType = '" + idTestType + "' ";

		testTypeModel = (TestTypeModel) session.createQuery(query + kondisi).uniqueResult();

		return testTypeModel;
	}

	// ============================= Update ============================= //
	@Override
	public void update(TestTypeModel testTypeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(testTypeModel);
	}

	// ============================= Search ============================= //
	@Override
	public List<TestTypeModel> searchNamaLike(String namaTestType) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<TestTypeModel> testTypeModelList = new ArrayList<TestTypeModel>();

		String query = " from TestTypeModel ";
		String kondisi = " where (namaTestType like '%" + namaTestType + "%') and isDelete !=1 ";

		testTypeModelList = session.createQuery(query + kondisi).list();

		return testTypeModelList;
	}

}
