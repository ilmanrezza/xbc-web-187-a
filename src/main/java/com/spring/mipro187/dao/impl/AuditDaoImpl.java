package com.spring.mipro187.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.AuditDao;
import com.spring.mipro187.model.AuditModel;

@Repository
public class AuditDaoImpl implements AuditDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(AuditModel auditModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(auditModel);
	}
	
}
