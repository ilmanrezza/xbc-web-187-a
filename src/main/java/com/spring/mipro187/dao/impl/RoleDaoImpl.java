package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.RoleDao;
import com.spring.mipro187.model.RoleModel;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<RoleModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		String query = " from RoleModel ";
		String kondisi = " where isDelete != 1 ";

		roleModelList = session.createQuery(query + kondisi).list();

		return roleModelList;
	}

}
