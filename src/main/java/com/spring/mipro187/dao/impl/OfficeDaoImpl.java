package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.OfficeDao;
import com.spring.mipro187.model.OfficeModel;
import com.spring.mipro187.model.RoomModel;

@Repository
public class OfficeDaoImpl implements OfficeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(officeModel);
	}

	@Override
	public List<OfficeModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>();

		String query = " from OfficeModel ";
		String kondisi = " where isDelete != 1 ";

		officeModelList = session.createQuery(query + kondisi).list();

		return officeModelList;
	}

	@Override
	public OfficeModel searchId(String idOffice) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		OfficeModel officeModel = new OfficeModel();

		String query = " from OfficeModel ";
		String kondisi = " where idOffice = '" + idOffice + "' ";

		officeModel = (OfficeModel) session.createQuery(query + kondisi).uniqueResult();

		return officeModel;
	}

	@Override
	public void update(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(officeModel);
	}

	// ====================== Room ====================== //

	@Override
	public void createRoom(RoomModel roomModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(roomModel);
	}

	@Override
	public List<RoomModel> searchAllRoom() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<RoomModel> roomModelList = new ArrayList<RoomModel>();

		String query = " from RoomModel ";
		String kondisi = " where isDelete != 1 ";

		roomModelList = session.createQuery(query + kondisi).list();

		return roomModelList;
	}

	@Override
	public RoomModel searchIdRoom(String idRoom) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		RoomModel roomModel = new RoomModel();

		String query = " from RoomModel ";
		String kondisi = " where idRoom = '" + idRoom + "' ";

		roomModel = (RoomModel) session.createQuery(query + kondisi).uniqueResult();

		return roomModel;
	}

}
