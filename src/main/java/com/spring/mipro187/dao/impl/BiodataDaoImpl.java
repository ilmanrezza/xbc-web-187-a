package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.BiodataDao;
import com.spring.mipro187.model.BiodataModel;

@Repository
public class BiodataDaoImpl implements BiodataDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(biodataModel);
	}

	@Override
	public List<BiodataModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();

		String query = " from BiodataModel ";
		String kondisi = " where isDelete = false ORDER BY name ";

		biodataModelList = session.createQuery(query + kondisi).list();
		return biodataModelList;
	}

	@Override
	public BiodataModel searchKode(String id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		BiodataModel biodataModel = new BiodataModel();
		String query = " from BiodataModel ";
		String kondisi = " where id = '" + id + "' ";
		biodataModel = (BiodataModel) session.createQuery(query + kondisi).uniqueResult();

		return biodataModel;
	}

	@Override
	public void update(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(biodataModel);
	}

	@Override
	public void delete(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(biodataModel);
	}

	@Override
	public List<BiodataModel> searchNameOrMajors(String name, String majors) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		String query = " from BiodataModel ";
		String kondisi = " where name like '%"+name+"%' or majors like'%"+majors+"%'";
		biodataModelList = session.createQuery(query + kondisi).list();

		return biodataModelList;
	}

}
