package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.UserDao;
import com.spring.mipro187.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<UserModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<UserModel> userModelList = new ArrayList<UserModel>();

		String query = " from UserModel ";
		String kondisi = " where isDelete != 1 ";

		userModelList = session.createQuery(query + kondisi).list();

		return userModelList;
	}

	@Override
	public UserModel searchId(String idUser) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		UserModel userModel = new UserModel();

		String query = " from UserModel ";
		String kondisi = " where idUser = '" + idUser + "' ";

		userModel = (UserModel) session.createQuery(query + kondisi).uniqueResult();

		return userModel;
	}

	@Override
	public void create(UserModel userModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(userModel);
	}

	@Override
	public void update(UserModel userModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userModel);
	}

	@Override
	public List<UserModel> searchUsernameEmail(String username, String email) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<UserModel> userModelList = new ArrayList<UserModel>();

		String query = " from UserModel ";
		String kondisi = " where (username like '%" + username + "%' or email like '%" + email + "%') and IS_DELETE = 0";

		userModelList = session.createQuery(query + kondisi).list();

		return userModelList;
	}

}
