package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.MonitoringDao;
import com.spring.mipro187.model.MonitoringModel;

@Repository
public class MonitoringDaoImpl implements MonitoringDao{

	@Autowired
	private SessionFactory sessionFactory;

	// ============================= Create ============================= //
	@Override
	public void create(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(monitoringModel);
	}

	// ============================= List ============================= //
	@Override
	public List<MonitoringModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		String query = " from MonitoringModel ";
		String kondisi = " where isDelete != 1 ";
		
		monitoringModelList = session.createQuery(query + kondisi).list();
		
		return monitoringModelList;
	}

	// ============================= Search Id ============================= // // *untuk ubah data
	@Override
	public MonitoringModel searchId(String idMonitoring) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		MonitoringModel monitoringModel = new MonitoringModel();
		
		String query = " from MonitoringModel ";
		String kondisi = " where idMonitoring = '"+idMonitoring+"' ";
		
		monitoringModel = (MonitoringModel) session.createQuery(query+kondisi).uniqueResult();
		
		return monitoringModel;
	}

	// ============================= update placement ============================= //
	@Override
	public void updatePlacement(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(monitoringModel);
	}

	// ============================= update data idle ============================= //
	@Override
	public void update(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(monitoringModel);
	}

	// ============================= Search by name ============================= //
	@Override
	public List<MonitoringModel> searchNamaLike(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();
		
		String query = " from MonitoringModel ";
		String kondisi = " where (biodataModel.name like '%"+name+"%') and isDelete != 1";
		
		monitoringModelList = session.createQuery(query + kondisi).list();
		
		return monitoringModelList;
	}

}
