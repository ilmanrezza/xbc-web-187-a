package com.spring.mipro187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mipro187.dao.BootcamptesDao;
import com.spring.mipro187.model.BootcamptesModel;

@Repository
public class BootcamptesDaoImpl implements BootcamptesDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(bootcamptesModel);
	}

	@Override
	public void update(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(bootcamptesModel);
	}

	@Override
	public void delete(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(bootcamptesModel);
	}

	@Override
	public List<BootcamptesModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BootcamptesModel> bootcamptesModel = new ArrayList<BootcamptesModel>();
		String query = "from BootcamptesModel";
		String kondisi = " where isDelete = false ORDER BY name ";
		bootcamptesModel = session.createQuery(query+kondisi).list();
		return bootcamptesModel;
	}

	@Override
	public BootcamptesModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		BootcamptesModel bootcamptesModel = new BootcamptesModel();
		String query = "from BootcamptesModel";
		String kondisi = " where id = '"+id+"' "; //query
		bootcamptesModel = (BootcamptesModel) session.createQuery(query + kondisi).uniqueResult();
		return bootcamptesModel;
	}

	@Override
	public List<BootcamptesModel> name(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BootcamptesModel> bootcamptesModel = new ArrayList<BootcamptesModel>();
		String query = "from BootcamptesModel";
		String kondisi = " where name like '%"+name+"%' "; //query
		bootcamptesModel = session.createQuery(query+kondisi).list();
		return bootcamptesModel;
	}

	/*
	 * @Override public List<BootcamptesModel> search(String kodeRole) { // TODO
	 * Auto-generated method stub Session session =
	 * this.sessionFactory.getCurrentSession(); List<BootcamptesModel>
	 * bootcamptesModel = new ArrayList<BootcamptesModel>();
	 * 
	 * String query = " from BootcamptesModel "; String kondisi =
	 * " where isDelete != true ";
	 * 
	 * if (kodeRole.equals("ROLE_ADMIN")) { kondisi = " "; } else {
	 * 
	 * } bootcamptesModel = session.createQuery(query+kondisi).list(); return
	 * bootcamptesModel; }
	 */


}
