package com.spring.mipro187.dao;

import java.util.List;

import com.spring.mipro187.model.UserModel;

public interface UserDao {

	public List<UserModel> searchAll();

	public UserModel searchId(String idUser);

	public void create(UserModel userModel);
	
	public void update(UserModel userModel);
	
	public List<UserModel> searchUsernameEmail(String username, String email);

}
