package com.spring.mipro187.dao;

import java.util.List;

import com.spring.mipro187.model.BiodataModel;

public interface BiodataDao {
	public void create(BiodataModel biodataModel);

	public List<BiodataModel> searchAll();

	public BiodataModel searchKode(String id);

	public void update(BiodataModel biodataModel);

	public void delete(BiodataModel biodataModel);

	public List<BiodataModel> searchNameOrMajors(String name, String majors);
}
