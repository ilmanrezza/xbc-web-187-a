package com.spring.mipro187.dao;

import java.util.List;

import com.spring.mipro187.model.RoleModel;

public interface RoleDao {

	public List<RoleModel> searchAll();
}
