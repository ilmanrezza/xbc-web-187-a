package com.spring.mipro187.dao;

import java.util.List;

import com.spring.mipro187.model.MonitoringModel;

public interface MonitoringDao {

	public List<MonitoringModel> searchAll();

	public void create(MonitoringModel monitoringModel);
	
	public MonitoringModel searchId(String idMonitoring); // primary key dibutuhkan untuk update data

	public void updatePlacement(MonitoringModel monitoringModel);
	
	public void update(MonitoringModel monitoringModel);
	
	public List<MonitoringModel> searchNamaLike(String name);
	
}
