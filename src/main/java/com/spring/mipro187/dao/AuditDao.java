package com.spring.mipro187.dao;

import com.spring.mipro187.model.AuditModel;

public interface AuditDao {

	public void create(AuditModel auditModel);

}
