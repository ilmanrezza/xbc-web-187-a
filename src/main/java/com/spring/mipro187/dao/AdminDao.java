package com.spring.mipro187.dao;

import com.spring.mipro187.model.UserModel;

public interface AdminDao {

	public UserModel searchUsernamePassword(String username, String password);
}
