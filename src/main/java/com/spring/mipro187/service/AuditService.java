package com.spring.mipro187.service;

import com.spring.mipro187.model.AuditModel;

public interface AuditService {

	public void create(AuditModel auditModel);
}
