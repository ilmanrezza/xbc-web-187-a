package com.spring.mipro187.service;

import java.util.List;

import com.spring.mipro187.model.RoleModel;

public interface RoleService {

	public List<RoleModel> searchAll();
}
