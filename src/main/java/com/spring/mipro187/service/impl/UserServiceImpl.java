package com.spring.mipro187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.UserDao;
import com.spring.mipro187.model.UserModel;
import com.spring.mipro187.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public List<UserModel> searchAll() {
		// TODO Auto-generated method stub
		return this.userDao.searchAll();
	}

	@Override
	public UserModel searchId(String idUser) {
		// TODO Auto-generated method stub
		return this.userDao.searchId(idUser);
	}

	@Override
	public void create(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.create(userModel);
	}

	@Override
	public void update(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.update(userModel);
	}

	@Override
	public void resetPassword(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.update(userModel);
	}

	@Override
	public void deleteTemporary(UserModel userModel) {
		// TODO Auto-generated method stub
		this.userDao.update(userModel);
	}

	@Override
	public List<UserModel> searchUsernameEmail(String username, String email) {
		// TODO Auto-generated method stub
		return this.userDao.searchUsernameEmail(username, email);
	}

}
