package com.spring.mipro187.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.mipro187.dao.BootcamptesDao;
import com.spring.mipro187.model.BootcamptesModel;
import com.spring.mipro187.service.BootcamptesService;

@Service
@Transactional
public class BootcamptesServiceImpl implements BootcamptesService{

	@Autowired
	private BootcamptesDao bootcamptesDao;
	
	@Override
	public void create(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		this.bootcamptesDao.create(bootcamptesModel);
	}

	@Override
	public void update(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		this.bootcamptesDao.update(bootcamptesModel);
	}

	@Override
	public void delete(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		this.bootcamptesDao.delete(bootcamptesModel);
	}

	@Override
	public List<BootcamptesModel> searchAll() {
		// TODO Auto-generated method stub
		return this.bootcamptesDao.searchAll();
	}



	@Override
	public List<BootcamptesModel> name(String name) {
		// TODO Auto-generated method stub
		return this.bootcamptesDao.name(name);
	}

	@Override
	public void deleteTemporary(BootcamptesModel bootcamptesModel) {
		// TODO Auto-generated method stub
		this.bootcamptesDao.update(bootcamptesModel);
	}

	@Override
	public BootcamptesModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.bootcamptesDao.searchId(id);
	}

	/*
	 * @Override public List<BootcamptesModel> search(String kodeRole) { // TODO
	 * Auto-generated method stub return this.bootcamptesDao.search(kodeRole); }
	 */

	

}
