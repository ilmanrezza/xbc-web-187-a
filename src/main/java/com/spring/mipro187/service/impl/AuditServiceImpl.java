package com.spring.mipro187.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.AuditDao;
import com.spring.mipro187.model.AuditModel;
import com.spring.mipro187.service.AuditService;

@Service
@Transactional
public class AuditServiceImpl implements AuditService {

	@Autowired
	private AuditDao auditDao;
	
	@Override
	public void create(AuditModel auditModel) {
		// TODO Auto-generated method stub
		this.auditDao.create(auditModel);
	}

}
