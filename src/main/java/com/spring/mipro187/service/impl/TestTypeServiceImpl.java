package com.spring.mipro187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.TestTypeDao;
import com.spring.mipro187.model.TestTypeModel;
import com.spring.mipro187.service.TestTypeService;

@Service
@Transactional
public class TestTypeServiceImpl implements TestTypeService {

	@Autowired
	private TestTypeDao testTypeDao;

	@Override
	public List<TestTypeModel> searchAll() {
		// TODO Auto-generated method stub
		return this.testTypeDao.searchAll();
	}

	@Override
	public void create(TestTypeModel testTypeModel) {
		// TODO Auto-generated method stub
		this.testTypeDao.create(testTypeModel);
	}


	@Override
	public TestTypeModel searchId(String idTestType) {
		// TODO Auto-generated method stub
		return this.testTypeDao.searchId(idTestType);
	}
	
	@Override
	public void update(TestTypeModel testTypeModel) {
		// TODO Auto-generated method stub
		this.testTypeDao.update(testTypeModel);
	}

	@Override
	public void deleteTemporary(TestTypeModel testTypeModel) {
		// TODO Auto-generated method stub
		this.testTypeDao.update(testTypeModel);
	}

	@Override
	public List<TestTypeModel> searchNamaLike(String namaTestType) {
		// TODO Auto-generated method stub
		return this.testTypeDao.searchNamaLike(namaTestType);
	}

}
