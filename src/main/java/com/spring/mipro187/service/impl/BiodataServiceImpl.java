package com.spring.mipro187.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.mipro187.dao.BiodataDao;
import com.spring.mipro187.model.BiodataModel;
import com.spring.mipro187.service.BiodataService;

@Service
@Transactional
public class BiodataServiceImpl implements BiodataService {
	@Autowired
	private BiodataDao biodataDao;

	@Override
	public void create(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.create(biodataModel);
	}

	@Override
	public List<BiodataModel> searchAll() {
		// TODO Auto-generated method stub
		return this.biodataDao.searchAll();
	}

	@Override
	public BiodataModel searchKode(String id) {
		// TODO Auto-generated method stub
		return this.biodataDao.searchKode(id);
	}

	@Override
	public void update(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.update(biodataModel);
	}

	@Override
	public void delete(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		this.biodataDao.delete(biodataModel);
	}

	@Override
	public List<BiodataModel> searchNameOrMajors(String name, String majors) {
		// TODO Auto-generated method stub
		return this.biodataDao.searchNameOrMajors(name, majors);
	}
}
