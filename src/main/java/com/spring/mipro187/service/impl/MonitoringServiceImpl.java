package com.spring.mipro187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.MonitoringDao;
import com.spring.mipro187.model.MonitoringModel;
import com.spring.mipro187.service.MonitoringService;

@Service
@Transactional
public class MonitoringServiceImpl implements MonitoringService{

	@Autowired
	private MonitoringDao monitoringDao;

	@Override
	public void create(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.create(monitoringModel);
	}

	@Override
	public List<MonitoringModel> searchAll() {
		// TODO Auto-generated method stub
		return this.monitoringDao.searchAll();
	}

	@Override
	public MonitoringModel searchId(String idMonitoring) {
		// TODO Auto-generated method stub
		return this.monitoringDao.searchId(idMonitoring);
	}

	@Override
	public void updatePlacement(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.updatePlacement(monitoringModel);
	}

	@Override
	public void update(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.update(monitoringModel);
	}

	@Override
	public void deleteTemporary(MonitoringModel monitoringModel) {
		// TODO Auto-generated method stub
		this.monitoringDao.update(monitoringModel);
	}

	@Override
	public List<MonitoringModel> searchNamaLike(String name) {
		// TODO Auto-generated method stub
		return this.monitoringDao.searchNamaLike(name);
	}

	

}
