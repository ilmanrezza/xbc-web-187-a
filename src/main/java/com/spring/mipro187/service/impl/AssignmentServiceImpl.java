package com.spring.mipro187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.AssignmentDao;
import com.spring.mipro187.model.AssignmentModel;
import com.spring.mipro187.service.AssignmentService;

@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	private AssignmentDao assignmentDao;
	
	@Override
	public List<AssignmentModel> searchAll() {
		// TODO Auto-generated method stub
		return this.assignmentDao.searchAll();
	}

	@Override
	public void create(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		this.assignmentDao.create(assignmentModel);
	}

	@Override
	public AssignmentModel searchId(String idAssignment) {
		// TODO Auto-generated method stub
		return this.assignmentDao.searchId(idAssignment);
	}

	@Override
	public void update(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		this.assignmentDao.update(assignmentModel);
	}
	
	@Override
	public void updateMark(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		this.assignmentDao.update(assignmentModel);
	}

	@Override
	public void deleteTemporary(AssignmentModel assignmentModel) {
		// TODO Auto-generated method stub
		this.assignmentDao.update(assignmentModel);
	}

	@Override
	public List<AssignmentModel> searchByDate(String date) {
		// TODO Auto-generated method stub
		return this.assignmentDao.searchByDate(date);
	}
	
}
