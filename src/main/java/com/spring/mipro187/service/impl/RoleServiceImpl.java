package com.spring.mipro187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.RoleDao;
import com.spring.mipro187.model.RoleModel;
import com.spring.mipro187.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<RoleModel> searchAll() {
		// TODO Auto-generated method stub
		return this.roleDao.searchAll();
	}

}
