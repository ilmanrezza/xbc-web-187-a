package com.spring.mipro187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.OfficeDao;
import com.spring.mipro187.model.OfficeModel;
import com.spring.mipro187.model.RoomModel;
import com.spring.mipro187.service.OfficeService;

@Service
@Transactional
public class OfficeServiceImpl implements OfficeService {

	@Autowired
	private OfficeDao officeDao;

	@Override
	public void create(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.create(officeModel);
	}

	@Override
	public List<OfficeModel> searchAll() {
		// TODO Auto-generated method stub
		return this.officeDao.searchAll();
	}

	@Override
	public OfficeModel searchId(String idOffice) {
		// TODO Auto-generated method stub
		return this.officeDao.searchId(idOffice);
	}

	@Override
	public void update(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.update(officeModel);
	}

	@Override
	public void deleteTemporary(OfficeModel officeModel) {
		// TODO Auto-generated method stub
		this.officeDao.update(officeModel);
	}

	// ====================== Room ====================== //

	@Override
	public void createRoom(RoomModel roomModel) {
		// TODO Auto-generated method stub
		this.officeDao.createRoom(roomModel);
	}

	@Override
	public List<RoomModel> searchAllRoom() {
		// TODO Auto-generated method stub
		return this.officeDao.searchAllRoom();
	}

	@Override
	public RoomModel searchIdRoom(String idRoom) {
		// TODO Auto-generated method stub
		return this.officeDao.searchIdRoom(idRoom);
	}

}
