package com.spring.mipro187.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mipro187.dao.AdminDao;
import com.spring.mipro187.model.UserModel;
import com.spring.mipro187.service.AdminService;

@Service
@Transactional
public class AdminServiceImpl implements AdminService{
	
	@Autowired
	private AdminDao userDao;

	@Override
	public UserModel searchUsernamePassword(String username, String password) {
		// TODO Auto-generated method stub
		return this.userDao.searchUsernamePassword(username, password);
	}

}
