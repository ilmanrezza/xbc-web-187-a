package com.spring.mipro187.service;

import java.util.List;

import com.spring.mipro187.model.OfficeModel;
import com.spring.mipro187.model.RoomModel;

public interface OfficeService {

	public List<OfficeModel> searchAll();

	public void create(OfficeModel officeModel);
	
	public OfficeModel searchId(String idOffice);
	
	public void update(OfficeModel officeModel);
	
	public void deleteTemporary(OfficeModel officeModel);
	
	
	// ====================== Room ====================== //
	
	public List<RoomModel> searchAllRoom();
	
	public void createRoom(RoomModel roomModel);
	
	public RoomModel searchIdRoom(String idRoom);
	
}
