package com.spring.mipro187.service;

import java.util.List;

import com.spring.mipro187.model.BootcamptesModel;


public interface BootcamptesService {
	public void create(BootcamptesModel bootcamptesModel);
	public void update(BootcamptesModel bootcamptesModel);
	public void delete(BootcamptesModel bootcamptesModel);
	public List<BootcamptesModel> searchAll();
	public BootcamptesModel searchId(Integer id);
	public List<BootcamptesModel> name(String name); //untuk search name di front end
	public void deleteTemporary(BootcamptesModel bootcamptesModel);
	//public List<BootcamptesModel> search(String kodeRole);
}
