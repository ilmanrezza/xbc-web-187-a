package com.spring.mipro187.service;

import java.util.List;

import com.spring.mipro187.model.TestTypeModel;

public interface TestTypeService {

	public List<TestTypeModel> searchAll();
	
	public TestTypeModel searchId(String idTestType);
	
	public void create(TestTypeModel testTypeModel);
	
	public void update(TestTypeModel testTypeModel);
	
	public void deleteTemporary(TestTypeModel testTypeModel);
	
	public List<TestTypeModel> searchNamaLike(String namaTestType);
}
