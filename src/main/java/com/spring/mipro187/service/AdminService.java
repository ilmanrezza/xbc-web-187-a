package com.spring.mipro187.service;

import com.spring.mipro187.model.UserModel;

public interface AdminService {

	public UserModel searchUsernamePassword(String username, String password);
}
