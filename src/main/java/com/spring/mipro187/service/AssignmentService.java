package com.spring.mipro187.service;

import java.util.List;

import com.spring.mipro187.model.AssignmentModel;

public interface AssignmentService {

	public List<AssignmentModel> searchAll();
	
	public void create(AssignmentModel assignmentModel);
	
	public AssignmentModel searchId(String idAssignment);
	
	public void update(AssignmentModel assignmentModel);
	
	public void updateMark(AssignmentModel assignmentModel);
	
	public void deleteTemporary(AssignmentModel assignmentModel);
	
	public List<AssignmentModel> searchByDate(String date);
	
}
