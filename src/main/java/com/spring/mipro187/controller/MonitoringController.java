package com.spring.mipro187.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.service.MonitoringService;
import com.spring.mipro187.model.AuditModel;
import com.spring.mipro187.model.BiodataModel;
import com.spring.mipro187.model.MonitoringModel;
import com.spring.mipro187.service.AuditService;
import com.spring.mipro187.service.BiodataService;

@Controller
public class MonitoringController extends AdminController {

	@Autowired
	private MonitoringService monitoringService;

	@Autowired
	private BiodataService biodataService;

	@Autowired
	private AuditService auditService;

	// =================== Home ===================//
	@RequestMapping(value = "monitoring") // url action
	public String monitoring() { // method

		String jsp = "monitoring/home"; // target atau halaman
		return jsp;
	}

	// =================== Input Idle ===================//
	@RequestMapping(value = "monitoring/add") // url action
	public String monitoringAdd(Model model) { // method

		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchAll();
		model.addAttribute("biodataModelList", biodataModelList);

		String jsp = "monitoring/add"; // target atau halaman
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "monitoring/create")
	public String monitoringCreate(HttpServletRequest request) throws ParseException {

		Integer idBiodata = Integer.valueOf(request.getParameter("idBiodata"));
		Date idleDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("idleDate"));
		String lastProject = request.getParameter("lastProject");
		String idleReason = request.getParameter("idleReason");

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel.setIdBiodata(idBiodata);
		monitoringModel.setIdleDate(idleDate);
		monitoringModel.setLastProject(lastProject);
		monitoringModel.setIdleReason(idleReason);

		monitoringModel.setIsDelete(false); // 0 artinya tidak didelete

		// save audit trail untuk created
		Integer IdCreatedBy = this.userSearch().getIdUser();
		monitoringModel.setIdCreatedBy(IdCreatedBy);
		monitoringModel.setCreatedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("INSERT");
		auditModel.setJsonInsert("{ 'idBiodata' : '" + idBiodata + "' , 'idleDate' : '" + idleDate
				+ "' , 'lastProject' : '" + lastProject + "' , 'idleReason':'" + idleReason + "'}");
		auditModel.setIdCreatedBy(IdCreatedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.monitoringService.create(monitoringModel);

		String jsp = "monitoring/home";
		return jsp;
	}

	// =================== List ===================//
	@RequestMapping(value = "monitoring/list")
	public String monitoringList(Model model) {

		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();

		monitoringModelList = this.monitoringService.searchAll();
		model.addAttribute("monitoringModelList", monitoringModelList);

		String jsp = "monitoring/list";
		return jsp;
	}

	// =================== Update Placement ===================//
	@RequestMapping(value = "monitoring/placement")
	public String monitoringEditPlacement(Model model, HttpServletRequest request) {
		String idMonitoring = request.getParameter("idMonitoring"); // untuk update data, primary key wajib di panggil

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel = this.monitoringService.searchId(idMonitoring);

		model.addAttribute("monitoringModel", monitoringModel);

		String jsp = "monitoring/placement";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "monitoring/updatePlacement")
	public String monitoringUpdatePlacement(Model model, HttpServletRequest request) throws ParseException {
		String idMonitoring = request.getParameter("idMonitoring");

		String jsp;
		Date placementDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("placementDate"));
		String placementAt = request.getParameter("placementAt");
		String notes = request.getParameter("notes");

		Date idleDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("idleDate")); // untuk fungsi

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel = this.monitoringService.searchId(idMonitoring);

		monitoringModel.setPlacementDate(placementDate);
		monitoringModel.setPlacementAt(placementAt);
		monitoringModel.setNotes(notes);

		if (placementDate.compareTo(idleDate) > 0) {
			this.monitoringService.updatePlacement(monitoringModel);
			model.addAttribute("kondisi", 1);
			jsp = "monitoring/home";
		} else {
			model.addAttribute("kondisi", 0);
			jsp = "monitoring/placement";
		}

		return jsp;
	}

	// =================== Edit Data ===================//
	@RequestMapping(value = "monitoring/edit")
	public String monitoringEdit(Model model, HttpServletRequest request) {
		String idMonitoring = request.getParameter("idMonitoring"); // untuk update data, primary key wajib di panggil

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel = this.monitoringService.searchId(idMonitoring);

		model.addAttribute("monitoringModel", monitoringModel);

		String jsp = "monitoring/edit";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "monitoring/update")
	public String monitoringUpdate(Model model, HttpServletRequest request) throws ParseException {
		String idMonitoring = request.getParameter("idMonitoring");

		String jsp;
		Date idleDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("editIdleDate"));
		String lastProject = request.getParameter("editLastProject");
		String idleReason = request.getParameter("editIdleReason");
		Date placementDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("editPlacementDate"));
		String placementAt = request.getParameter("editPlacementAt");
		String notes = request.getParameter("editNotes");

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel = this.monitoringService.searchId(idMonitoring);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		Date idleDateOld = monitoringModel.getIdleDate();
		String lastProjectOld = monitoringModel.getLastProject();
		String idleReasonOld = monitoringModel.getIdleReason();
		Date placementDateOld = monitoringModel.getPlacementDate();
		String placementAtOld = monitoringModel.getPlacementAt();
		String notesOld = monitoringModel.getNotes();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		monitoringModel.setIdleDate(idleDate);
		monitoringModel.setLastProject(lastProject);
		monitoringModel.setIdleReason(idleReason);
		monitoringModel.setPlacementDate(placementDate);
		monitoringModel.setPlacementAt(placementAt);
		monitoringModel.setNotes(notes);

		// save audit trail untuk updated
		Integer IdModifiedBy = this.userSearch().getIdUser();
		monitoringModel.setIdModifiedBy(IdModifiedBy);
		monitoringModel.setModifiedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("MODIFY");
		auditModel.setJsonBefore("{ 'idleDate' : '" + idleDateOld + "' , 'lastProject' : '" + lastProjectOld
				+ "' , 'idleReason' : '" + idleReasonOld + "' , 'placementDate':'" + placementDateOld
				+ "' , 'placementAt':'" + placementAtOld + "' , 'notes':'" + notesOld + "'}");

		auditModel.setJsonAfter("{ 'idleDate' : '" + idleDate + "' , 'lastProject' : '" + lastProject
				+ "' , 'idleReason' : '" + idleReason + "' , 'placementDate':'" + placementDate + "' , 'placementAt':'"
				+ placementAt + "' , 'notes':'" + notes + "'}");

		auditModel.setIdCreatedBy(IdModifiedBy);
		auditModel.setCreatedOn(new Date());
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		if (placementDate.compareTo(idleDate) > 0) {
			// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
			this.auditService.create(auditModel);
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			this.monitoringService.update(monitoringModel);
			model.addAttribute("kondisi", 1);
			jsp = "monitoring/home";
		} else {
			model.addAttribute("kondisi", 0);
			jsp = "monitoring/placement";
		}

		return jsp;
	}

	// =================== Remove Data ===================//
	@RequestMapping(value = "monitoring/remove")
	public String monitoringRemove(Model model, HttpServletRequest request) {
		String idMonitoring = request.getParameter("idMonitoring");

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel = this.monitoringService.searchId(idMonitoring);

		model.addAttribute("monitoringModel", monitoringModel);

		String jsp = "monitoring/remove";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "monitoring/deleteTemporary")
	public String monitoringDeleteTemporary(HttpServletRequest request) {
		String idMonitoring = request.getParameter("idMonitoring");

		MonitoringModel monitoringModel = new MonitoringModel();

		monitoringModel = this.monitoringService.searchId(idMonitoring);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		Date idleDateOld = monitoringModel.getIdleDate();
		String lastProjectOld = monitoringModel.getLastProject();
		String idleReasonOld = monitoringModel.getIdleReason();
		Date placementDateOld = monitoringModel.getPlacementDate();
		String placementAtOld = monitoringModel.getPlacementAt();
		String notesOld = monitoringModel.getNotes();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		monitoringModel.setIsDelete(true);

		// save audit trail untuk deleted
		Integer IdDeletedBy = this.userSearch().getIdUser();
		monitoringModel.setIdDeletedBy(IdDeletedBy);
		monitoringModel.setDeletedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("DELETE");
		auditModel.setJsonDelete("{ 'idleDate' : '" + idleDateOld + "' , 'lastProject' : '" + lastProjectOld
				+ "' , 'idleReason' : '" + idleReasonOld + "' , 'placementDate':'" + placementDateOld
				+ "' , 'placementAt':'" + placementAtOld + "' , 'notes':'" + notesOld + "'}");
		auditModel.setIdCreatedBy(IdDeletedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.monitoringService.update(monitoringModel);

		String jsp = "monitoring/home";
		return jsp;
	}

	// =================== Search by Name ===================//
	@RequestMapping(value = "monitoring/search")
	public String biodataSearch(Model model, HttpServletRequest request) {
		String name = request.getParameter("search");

		List<MonitoringModel> monitoringModelList = new ArrayList<MonitoringModel>();

		monitoringModelList = this.monitoringService.searchNamaLike(name);

		model.addAttribute("monitoringModelList", monitoringModelList);

		String jsp = "monitoring/list";
		return jsp;
	}

	// =================== Reset ===================//
	@RequestMapping(value = "monitoring/reset")
	public String monitoringCancelCreate() {

		String jsp = "monitoring/home";
		return jsp;
	}
}
