package com.spring.mipro187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.AuditModel;
import com.spring.mipro187.model.OfficeModel;
import com.spring.mipro187.model.RoomModel;
import com.spring.mipro187.service.AuditService;
import com.spring.mipro187.service.OfficeService;

@Controller
public class OfficeController extends AdminController {

	@Autowired
	private OfficeService officeService;

	@Autowired
	private AuditService auditService;

	@RequestMapping(value = "office")
	public String office() {

		String jsp = "office/home";
		return jsp;
	}

	// =================== List Office =================== //
	@RequestMapping(value = "office/list")
	public String officeList(Model model) {

		List<OfficeModel> officeModelList = new ArrayList<OfficeModel>();

		officeModelList = this.officeService.searchAll();

		model.addAttribute("officeModelList", officeModelList);

		String jsp = "office/list";
		return jsp;
	}

	// =================== Add Office =================== //
	@RequestMapping(value = "office/add")
	public String officeAdd() {

		String jsp = "office/add";
		return jsp;
	}

	// ============================================================================================

	@RequestMapping(value = "office/create")
	public String officeCreate(HttpServletRequest request) {
		String name = request.getParameter("officeName");
		String phone = request.getParameter("officePhone");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String description = request.getParameter("description");

		OfficeModel officeModel = new OfficeModel();

		officeModel.setNameOffice(name);
		officeModel.setPhoneOffice(phone);
		officeModel.setEmail(email);
		officeModel.setAddress(address);
		officeModel.setNotesOffice(description);

		officeModel.setIsDelete(false);

		Integer IdCreatedBy = this.userSearch().getIdUser();
		officeModel.setIdCreatedBy(IdCreatedBy);
		officeModel.setCreatedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("INSERT");
		auditModel.setJsonInsert("{ 'name' : '" + name + "' , 'phone' : '" + phone + "' , 'email' : '" + email
				+ "' , 'address':'" + address + "' , 'description':'" + description + "'}");
		auditModel.setIdCreatedBy(IdCreatedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.officeService.create(officeModel);

		String jsp = "office/home";
		return jsp;
	}

	// =================== Edit Office =================== //
	@RequestMapping(value = "office/edit")
	public String officeEdit(Model model, HttpServletRequest request) {
		String idOffice = request.getParameter("idOffice");

		OfficeModel officeModel = new OfficeModel();

		officeModel = this.officeService.searchId(idOffice);

		model.addAttribute("officeModel", officeModel);

		String jsp = "office/edit";
		return jsp;
	}

	// ============================================================================================

	@RequestMapping(value = "office/update")
	public String officeUpdate(HttpServletRequest request) {
		String idOffice = request.getParameter("idOffice");

		String name = request.getParameter("editOfficeName");
		String phone = request.getParameter("editOfficePhone");
		String email = request.getParameter("editEmail");
		String address = request.getParameter("editAddress");
		String description = request.getParameter("editDescription");

		OfficeModel officeModel = new OfficeModel();

		officeModel = this.officeService.searchId(idOffice);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		String nameOld = officeModel.getNameOffice();
		String phoneOld = officeModel.getPhoneOffice();
		String emailOld = officeModel.getEmail();
		String addressOld = officeModel.getAddress();
		String descriptionOld = officeModel.getNotesOffice();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		officeModel.setNameOffice(name);
		officeModel.setPhoneOffice(phone);
		officeModel.setEmail(email);
		officeModel.setAddress(address);
		officeModel.setNotesOffice(description);

		// save audit trail untuk updated
		Integer IdModifiedBy = this.userSearch().getIdUser();
		officeModel.setIdModifiedBy(IdModifiedBy);
		officeModel.setModifiedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("MODIFY");
		auditModel.setJsonBefore("{ 'name' : '" + name + "' , 'phone' : '" + phone + "' , 'email' : '" + email
				+ "' , 'address':'" + address + "' , 'description':'" + description + "'}");

		auditModel.setJsonAfter("{ 'name' : '" + nameOld + "' , 'phone' : '" + phoneOld + "' , 'email' : '" + emailOld
				+ "' , 'address':'" + addressOld + "' , 'description':'" + descriptionOld + "'}");

		auditModel.setIdCreatedBy(IdModifiedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.officeService.update(officeModel);

		String jsp = "office/home";
		return jsp;
	}

	// =================== Remove Office =================== //
	@RequestMapping(value = "office/remove")
	public String officeRemove(Model model, HttpServletRequest request) {
		String idOffice = request.getParameter("idOffice");

		OfficeModel officeModel = new OfficeModel();

		officeModel = this.officeService.searchId(idOffice);

		model.addAttribute("officeModel", officeModel);

		String jsp = "office/remove";
		return jsp;
	}

	// ============================================================================================

	@RequestMapping(value = "office/deleteTemporary")
	public String officeDeleteTemporary(HttpServletRequest request) {
		String idOffice = request.getParameter("idOffice");

		OfficeModel officeModel = new OfficeModel();

		officeModel = this.officeService.searchId(idOffice);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		String nameOld = officeModel.getNameOffice();
		String phoneOld = officeModel.getPhoneOffice();
		String emailOld = officeModel.getEmail();
		String addressOld = officeModel.getAddress();
		String descriptionOld = officeModel.getNotesOffice();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		officeModel.setIsDelete(true);

		// save audit trail untuk deleted
		Integer IdDeletedBy = this.userSearch().getIdUser();
		officeModel.setIdDeletedBy(IdDeletedBy);
		officeModel.setDeletedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("DELETE");
		auditModel.setJsonDelete("{ 'name' : '" + nameOld + "' , 'phone' : '" + phoneOld + "' , 'email' : '" + emailOld
				+ "' , 'address':'" + addressOld + "' , 'description':'" + descriptionOld + "'}");

		auditModel.setIdCreatedBy(IdDeletedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.officeService.update(officeModel);

		String jsp = "office/home";
		return jsp;
	}

	// =================== Reset Office =================== //
	@RequestMapping(value = "office/reset")
	public String officeCancel() {

		String jsp = "office/home";
		return jsp;
	}

	// ********************************* Room ********************************* //

	// =================== List Room =================== //
	@RequestMapping(value = "room/listRoom")
	public String roomList(Model model) {

		List<RoomModel> roomModelList = new ArrayList<RoomModel>();

		roomModelList = this.officeService.searchAllRoom();

		model.addAttribute("roomModelList", roomModelList);

		String jsp = "room/listRoom";
		return jsp;
	}

	// =================== Add Room =================== //
	@RequestMapping(value = "room/addRoom")
	public String roomAdd() {

		String jsp = "room/addRoom";
		return jsp;
	}

	// =================== Edit Room =================== //
	@RequestMapping(value = "room/editRoom")
	public String roomEdit(Model model, HttpServletRequest request) {
		String idRoom = request.getParameter("idRoom");

		RoomModel roomModel = new RoomModel();

		roomModel = this.officeService.searchIdRoom(idRoom);

		model.addAttribute("roomModel", roomModel);

		String jsp = "room/editRoom";
		return jsp;
	}

	// =================== Remove Room =================== //
	@RequestMapping(value = "room/removeRoom")
	public String roomRemove(Model model, HttpServletRequest request) {
		String idRoom = request.getParameter("idRoom");

		RoomModel roomModel = new RoomModel();

		roomModel = this.officeService.searchIdRoom(idRoom);

		model.addAttribute("roomModel", roomModel);

		String jsp = "room/removeRoom";
		return jsp;
	}

	// =================== Reset Room =================== //
	@RequestMapping(value = "room/reset")
	public String roomCancel() {

		String jsp = "office/add";
		return jsp;
	}
}
