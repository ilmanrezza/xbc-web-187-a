package com.spring.mipro187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.BiodataModel;
import com.spring.mipro187.model.BootcamptesModel;
import com.spring.mipro187.service.BiodataService;
import com.spring.mipro187.service.BootcamptesService;

@Controller
public class BiodataController extends AdminController {
	@Autowired
	private BiodataService biodataService;

	@Autowired
	private BootcamptesService bootcamptesService;

	@RequestMapping(value = "biodata")
	private String biodata() {
		String jsp = "biodata/biodata";
		return jsp;
	}

	@RequestMapping(value = "biodata/add")
	public String biodataAdd() {
		String jsp = "biodata/add";
		return jsp;
	}

	@RequestMapping(value = "biodata/create")
	public String biodataCreate(HttpServletRequest request) {
		// String id = String.valueOf(request.getParameter("id"));
		String name = request.getParameter("name");
		String lastEducation = request.getParameter("lastEducation");
		String educationalLevel = request.getParameter("educationalLevel");
		String graduationYear = request.getParameter("graduationYear");
		String majors = request.getParameter("majors");
		String gpa = request.getParameter("gpa");

		BiodataModel biodataModel = new BiodataModel();

		Integer idCreatedBy = this.userSearch().getIdUser(); // membuat var idCreatedBy, yang berisikan "mencari user
																// berdasarkan IDnya"
		biodataModel.setIdCreatedBy(idCreatedBy);
		biodataModel.setCreatedOn(new Date());

		// biodataModel.setId(Integer.valueOf(id));
		biodataModel.setName(name);
		biodataModel.setLastEducation(lastEducation);
		biodataModel.setEducationalLevel(educationalLevel);
		biodataModel.setGraduationYear(graduationYear);
		biodataModel.setMajors(majors);
		biodataModel.setGpa(gpa);

		biodataModel.setIsDelete(false);
		this.biodataService.create(biodataModel);

		String jsp = "biodata/biodata";
		return jsp;
	}

	@RequestMapping(value = "biodata/list")
	public String biodataList(Model model) {
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchAll();

		model.addAttribute("biodataModelList", biodataModelList);

		String jsp = "biodata/list";
		return jsp;
	}

	@RequestMapping(value = "biodata/edit")
	public String biodataEdit(Model model, HttpServletRequest request) {
		String id = String.valueOf(request.getParameter("id"));

		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchKode(id);

		model.addAttribute("biodataModel", biodataModel);

		List<BootcamptesModel> bootcamptesModelList = new ArrayList<BootcamptesModel>();
		bootcamptesModelList = this.bootcamptesService.searchAll();
		model.addAttribute("bootcamptesModelList", bootcamptesModelList);

		String jsp = "biodata/edit";
		return jsp;
	}

	@RequestMapping(value = "biodata/update")
	public String biodataUpdate(Model model, HttpServletRequest request) {
		String id = String.valueOf(request.getParameter("id"));
		String name = request.getParameter("name");
		String lastEducation = request.getParameter("lastEducation");
		String educationalLevel = request.getParameter("educationalLevel");
		String graduationYear = request.getParameter("graduationYear");
		String majors = request.getParameter("majors");
		String gpa = request.getParameter("gpa");
		String gender = request.getParameter("gender");
		String iq = request.getParameter("iq");
		String du = request.getParameter("du");
		String nestedLogic = request.getParameter("nestedLogic");
		String joinTable = request.getParameter("joinTable");
		String arithmetic = request.getParameter("arithmetic");
		String tro = request.getParameter("tro");
		String interviewer = request.getParameter("interviewer");
		String notes = request.getParameter("notes");
		String idBootcampTest = request.getParameter("idBootcampTest");

		// instance
		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchKode(id);

		biodataModel.setName(name);
		biodataModel.setLastEducation(lastEducation);
		biodataModel.setEducationalLevel(educationalLevel);
		biodataModel.setGraduationYear(graduationYear);
		biodataModel.setMajors(majors);
		biodataModel.setGpa(gpa);
		biodataModel.setGender(gender);
		biodataModel.setIq(Integer.valueOf(iq));
		biodataModel.setDu(du);
		biodataModel.setNestedLogic(Integer.valueOf(nestedLogic));
		biodataModel.setJoinTable(Integer.valueOf(joinTable));
		biodataModel.setArithmetic(Integer.valueOf(arithmetic));
		biodataModel.setTro(tro);
		biodataModel.setInterviewer(interviewer);
		biodataModel.setNotes(notes);
		biodataModel.setBootcampTestType(Integer.valueOf(idBootcampTest));

		Integer idModifiedBy = this.userSearch().getIdUser();
		biodataModel.setIdModifiedBy(idModifiedBy);
		biodataModel.setModifiedOn(new Date());

		this.biodataService.update(biodataModel);
		String jsp = "biodata/biodata";
		return jsp;
	}

	@RequestMapping(value = "biodata/remove")
	public String biodataDelete(Model model, HttpServletRequest request) {

		String id = String.valueOf(request.getParameter("id"));
		BiodataModel biodataModel = new BiodataModel();

		biodataModel = this.biodataService.searchKode(id);
		model.addAttribute("biodataModel", biodataModel);

		String jsp = "biodata/remove";
		return jsp;

	}

	@RequestMapping(value = "biodata/delete")
	public String biodataRemove(HttpServletRequest request) {
		String id = request.getParameter("id");
		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchKode(id);

		biodataModel.setIsDelete(true);

		// save audit trail
		Integer idDeletedBy = this.userSearch().getIdUser();
		biodataModel.setIdDeletedBy(idDeletedBy);
		biodataModel.setDeletedOn(new Date());

		// perintah delete
		this.biodataService.update(biodataModel);

		String jsp = "biodata/biodata";
		return jsp;
	}

	@RequestMapping(value = "biodata/search")
	public String biodataSearch(Model model, HttpServletRequest request) {
		String name = request.getParameter("nameOrMajors");
		String majors = request.getParameter("nameOrMajors");

		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchNameOrMajors(name, majors);

		model.addAttribute("biodataModelList", biodataModelList);

		String jsp = "biodata/list";
		return jsp;
	}
}
