package com.spring.mipro187.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;

import com.spring.mipro187.model.UserModel;
import com.spring.mipro187.service.AdminService;

@Controller
public class AdminController {
	
	@Autowired
	private AdminService userService;
	
	public UserModel userSearch() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserModel userModel = new UserModel();
		if (auth != null) {
			User user = (User) auth.getPrincipal();
			String username = user.getUsername();
			String password = user.getPassword();
			
			try {
				userModel = this.userService.searchUsernamePassword(username, password);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} else {

		}
		return userModel;
	}

}
