package com.spring.mipro187.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.AssignmentModel;
import com.spring.mipro187.model.AuditModel;
import com.spring.mipro187.model.BiodataModel;
import com.spring.mipro187.service.AssignmentService;
import com.spring.mipro187.service.AuditService;
import com.spring.mipro187.service.BiodataService;

@Controller
public class AssignmentController extends AdminController {

	@Autowired
	private AssignmentService assignmentService;

	@Autowired
	private BiodataService biodataService;

	@Autowired
	private AuditService auditService;

	// =================== Home ===================//
	@RequestMapping(value = "assignment")
	public String assignment() {

		String jsp = "assignment/home";
		return jsp;
	}

	// =================== List ===================//
	@RequestMapping(value = "assignment/list")
	public String assignmentList(Model model) {

		List<AssignmentModel> assignmentModelList = new ArrayList<AssignmentModel>();

		assignmentModelList = this.assignmentService.searchAll();
		model.addAttribute("assignmentModelList", assignmentModelList);

		String jsp = "assignment/list";
		return jsp;
	}

	// =================== Add ===================//
	@RequestMapping(value = "assignment/add")
	public String assignmentAdd(Model model) {

		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchAll();
		model.addAttribute("biodataModelList", biodataModelList);

		String jsp = "assignment/add";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "assignment/create")
	public String assignmentCreate(Model model, HttpServletRequest request) throws ParseException {
		String jsp;

		Integer idBiodata = Integer.valueOf(request.getParameter("idBiodata"));
		String title = request.getParameter("title");
		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("startDate"));
		Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("endDate"));
		String description = request.getParameter("description");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel.setIdBiodata(idBiodata);
		assignmentModel.setTitle(title);
		assignmentModel.setStartDate(startDate);
		assignmentModel.setEndDate(endDate);
		assignmentModel.setDescription(description);

		assignmentModel.setIsDelete(false);
		assignmentModel.setIsDone(false);
		assignmentModel.setIsHold(false);

		// save audit trail untuk created
		Integer IdCreatedBy = this.userSearch().getIdUser();
		assignmentModel.setIdCreatedBy(IdCreatedBy);
		assignmentModel.setCreatedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("INSERT");
		auditModel.setJsonInsert("{ 'title' : '" + title + "' , 'startDate' : '" + startDate + "' , 'endDate' : '"
				+ endDate + "' , 'description':'" + description + "'}");
		auditModel.setIdCreatedBy(IdCreatedBy);
		auditModel.setCreatedOn(new Date());
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		if (endDate.compareTo(startDate) > 0) {
			// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
			this.auditService.create(auditModel);
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			this.assignmentService.create(assignmentModel);
			model.addAttribute("kondisi", 1);
			jsp = "assignment/home";
		} else {
			model.addAttribute("kondisi", 0);
			jsp = "assignment/create";
		}

		return jsp;
	}

	// =================== Update ===================//
	@RequestMapping(value = "assignment/edit")
	public String assignmentEdit(Model model, HttpServletRequest request) {
		String idAssignment = request.getParameter("idAssignment"); // untuk update data, primary key wajib di panggil

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		model.addAttribute("assignmentModel", assignmentModel);

		String jsp = "assignment/edit";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "assignment/update")
	public String assignmentUpdate(Model model, HttpServletRequest request) throws ParseException {
		String idAssignment = request.getParameter("idAssignment");
		String jsp;

		String title = request.getParameter("editTitle");
		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("editStartDate"));
		Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("editEndDate"));
		String description = request.getParameter("editDescription");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		String titleOld = assignmentModel.getTitle();
		Date startDateOld = assignmentModel.getStartDate();
		Date endDateOld = assignmentModel.getEndDate();
		String descriptionOld = assignmentModel.getDescription();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		assignmentModel.setTitle(title);
		assignmentModel.setStartDate(startDate);
		assignmentModel.setEndDate(endDate);
		assignmentModel.setDescription(description);

		// save audit trail untuk updated
		Integer IdModifiedBy = this.userSearch().getIdUser();
		assignmentModel.setIdModifiedBy(IdModifiedBy);
		assignmentModel.setModifiedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("MODIFY");
		auditModel.setJsonBefore("{ 'title' : '" + titleOld + "' , 'startDate' : '" + startDateOld + "' , 'endDate' : '"
				+ endDateOld + "' , 'description':'" + descriptionOld + "'}");

		auditModel.setJsonAfter("{ 'title' : '" + title + "' , 'startDate' : '" + startDate + "' , 'endDate' : '"
				+ endDate + "' , 'description':'" + description + "'}");
		auditModel.setIdCreatedBy(IdModifiedBy);
		auditModel.setCreatedOn(new Date());
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		if (endDate.compareTo(startDate) > 0) {
			// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
			this.auditService.create(auditModel);
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			this.assignmentService.update(assignmentModel);
			model.addAttribute("kondisi", 1);
			jsp = "assignment/home";
		} else {
			model.addAttribute("kondisi", 0);
			jsp = "assignment/update";
		}

		return jsp;
	}

	// =================== Update Mark ===================//
	@RequestMapping(value = "assignment/mark")
	public String assignmentMark(Model model, HttpServletRequest request) {
		String idAssignment = request.getParameter("idAssignment"); // untuk update data, primary key wajib di panggil

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		model.addAttribute("assignmentModel", assignmentModel);

		String jsp = "assignment/mark";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "assignment/updateMark")
	public String assignmentUpdateMark(Model model, HttpServletRequest request) throws ParseException {
		String idAssignment = request.getParameter("idAssignment");

		Date realizationDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("realizationDate"));
		String notes = request.getParameter("notes");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		assignmentModel.setRealizationDate(realizationDate);
		assignmentModel.setNotes(notes);
		assignmentModel.setIsDone(true);

		this.assignmentService.update(assignmentModel);

		String jsp = "assignment/home";
		return jsp;
	}

	// =================== Remove Data ===================//
	@RequestMapping(value = "assignment/remove")
	public String assignmentRemove(Model model, HttpServletRequest request) {
		String idAssignment = request.getParameter("idAssignment");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		model.addAttribute("assignmentModel", assignmentModel);

		String jsp = "assignment/remove";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "assignment/deleteTemporary")
	public String assignmentDeleteTemporary(HttpServletRequest request) {
		String idAssignment = request.getParameter("idAssignment");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		String titleOld = assignmentModel.getTitle();
		Date startDateOld = assignmentModel.getStartDate();
		Date endDateOld = assignmentModel.getEndDate();
		String descriptionOld = assignmentModel.getDescription();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		assignmentModel.setIsDelete(true);

		// save audit trail untuk deleted
		Integer IdDeletedBy = this.userSearch().getIdUser();
		assignmentModel.setIdDeletedBy(IdDeletedBy);
		assignmentModel.setDeletedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("DELETE");
		auditModel.setJsonDelete("{ 'title' : '" + titleOld + "' , 'startDate' : '" + startDateOld + "' , 'endDate' : '"
				+ endDateOld + "' , 'description':'" + descriptionOld + "'}");
		auditModel.setIdCreatedBy(IdDeletedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.assignmentService.update(assignmentModel);

		String jsp = "assignment/home";
		return jsp;
	}

	// =================== Hold Data ===================//
	@RequestMapping(value = "assignment/hold")
	public String assignmentHold(Model model, HttpServletRequest request) {
		String idAssignment = request.getParameter("idAssignment");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		model.addAttribute("assignmentModel", assignmentModel);

		String jsp = "assignment/hold";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "assignment/isHold")
	public String assignmentIsHold(HttpServletRequest request) {
		String idAssignment = request.getParameter("idAssignment");

		AssignmentModel assignmentModel = new AssignmentModel();

		assignmentModel = this.assignmentService.searchId(idAssignment);

		assignmentModel.setIsHold(true);

		// save audit trail untuk deleted
		Integer IdDeletedBy = this.userSearch().getIdUser();
		assignmentModel.setIdDeletedBy(IdDeletedBy);
		assignmentModel.setDeletedOn(new Date());

		this.assignmentService.update(assignmentModel);

		String jsp = "assignment/home";
		return jsp;
	}

	// =================== Search by Date ===================//
	@RequestMapping(value = "assignment/search")
	public String assignmentSearch(Model model, HttpServletRequest request) {
		String date = request.getParameter("searchDate");

		List<AssignmentModel> assignmentModelList = new ArrayList<AssignmentModel>();

		assignmentModelList = this.assignmentService.searchByDate(date);

		model.addAttribute("assignmentModelList", assignmentModelList);

		String jsp = "assignment/list";
		return jsp;
	}

	// =================== Reset ===================//
	@RequestMapping(value = "assignment/reset")
	public String assignmentCancel() {

		String jsp = "assignment/home";
		return jsp;
	}

}
