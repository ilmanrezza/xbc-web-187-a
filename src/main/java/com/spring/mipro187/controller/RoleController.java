package com.spring.mipro187.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.RoleModel;
import com.spring.mipro187.service.RoleService;

@Controller
public class RoleController {

	@Autowired
	private RoleService roleService;

	// =================== List Role =================== //
	@RequestMapping(value = "role/listRole")
	public String roleList(Model model) {

		List<RoleModel> roleModelList = new ArrayList<RoleModel>();

		roleModelList = this.roleService.searchAll();

		model.addAttribute("roleModelList", roleModelList);

		String jsp = "role/listRole";
		return jsp;
	}
}
