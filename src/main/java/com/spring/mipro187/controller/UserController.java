package com.spring.mipro187.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.AuditModel;
import com.spring.mipro187.model.MonitoringModel;
import com.spring.mipro187.model.UserModel;
import com.spring.mipro187.model.UserModel;
import com.spring.mipro187.model.RoleModel;
import com.spring.mipro187.model.UserModel;
import com.spring.mipro187.service.AuditService;
import com.spring.mipro187.service.RoleService;
import com.spring.mipro187.service.UserService;

@Controller
public class UserController extends AdminController {

	@Autowired
	private UserService userService;

	@Autowired
	private AuditService auditService;

	@Autowired
	private RoleService roleService;

	// =================== Home ===================//
	@RequestMapping(value = "user")
	public String user() {

		String jsp = "user/home";
		return jsp;
	}

	// =================== List ===================//
	@RequestMapping(value = "user/list")
	public String userList(Model model) {

		List<UserModel> userModelList = new ArrayList<UserModel>();

		userModelList = this.userService.searchAll();

		model.addAttribute("userModelList", userModelList);

		String jsp = "user/list";
		return jsp;
	}

	// =================== Add ===================//
	@RequestMapping(value = "user/add")
	public String userAdd(Model model) {

		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.searchAll();
		model.addAttribute("roleModelList", roleModelList);

		String jsp = "user/add";
		return jsp;
	}

	// ============================================================================================//
	@RequestMapping(value = "user/create")
	public String userCreate(Model model, HttpServletRequest request) {
		String jsp;

		Integer idRole = Integer.valueOf(request.getParameter("idRole"));
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");

		UserModel userModel = new UserModel();

		userModel.setIdRole(idRole);
		userModel.setEmail(email);
		userModel.setUsername(username);
		userModel.setPassword(password);

		userModel.setMobileFlag(false);
		userModel.setisDelete(false);

		Integer IdCreatedBy = this.userSearch().getIdUser();
		userModel.setIdCreatedBy(IdCreatedBy);
		userModel.setCreatedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("INSERT");
		auditModel.setJsonInsert("{ 'idRole' : '" + idRole + "' , 'email' : '" + email + "'  , 'username' : '"
				+ username + "'  , 'password' : '" + password + "' }");
		auditModel.setIdCreatedBy(IdCreatedBy);
		auditModel.setCreatedOn(new Date());

		if (password.equals(rePassword)) {
			// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
			this.auditService.create(auditModel);
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			this.userService.create(userModel);
			model.addAttribute("kondisi", "1");
			jsp = "user/home";
		} else {
			model.addAttribute("kondisi", 0);
			jsp = "user/add";
		}

		return jsp;
	}

	// =================== Edit Data ===================//
	@RequestMapping(value = "user/edit")
	public String userEdit(Model model, HttpServletRequest request) {
		String idUser = request.getParameter("idUser"); // untuk update data, primary key wajib di panggil

		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.searchAll();
		model.addAttribute("roleModelList", roleModelList);

		UserModel userModel = new UserModel();

		userModel = this.userService.searchId(idUser);

		model.addAttribute("userModel", userModel);

		String jsp = "user/edit";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "user/update")
	public String userUpdate(Model model, HttpServletRequest request) throws ParseException {
		String idUser = request.getParameter("idUser");

		String jsp;
		Integer idRole = Integer.parseInt(request.getParameter("editIdRole"));
		String email = request.getParameter("editEmail");
		String username = request.getParameter("editUsername");
		Boolean mobileFlag = Boolean.parseBoolean(request.getParameter("mobileFlag"));

		UserModel userModel = new UserModel();

		userModel = this.userService.searchId(idUser);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		Integer idRoleOld = userModel.getIdRole();
		String emailOld = userModel.getEmail();
		String usernameOld = userModel.getUsername();
		Boolean mobileFlagOld = userModel.getMobileFlag();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		userModel.setIdRole(idRole);
		userModel.setEmail(email);
		userModel.setUsername(username);
		userModel.setMobileFlag(mobileFlag);

		// save audit trail untuk updated
		Integer IdModifiedBy = this.userSearch().getIdUser();
		userModel.setIdModifiedBy(IdModifiedBy);
		userModel.setModifiedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("MODIFY");
		auditModel.setJsonBefore("{ 'idRole' : '" + idRoleOld + "' , 'email' : '" + emailOld + "' , 'username' : '"
				+ usernameOld + "' , 'mobileFlag':'" + mobileFlagOld + "'}");

		auditModel.setJsonAfter("{ 'idRole' : '" + idRole + "' , 'email' : '" + email + "' , 'username' : '" + username
				+ "' , 'mobileFlag':'" + mobileFlag + "'}");

		auditModel.setIdCreatedBy(IdModifiedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.userService.update(userModel);

		jsp = "user/home";
		return jsp;
	}

	// =================== Reset Password ===================//
	@RequestMapping(value = "user/resetPassword")
	public String userResetPassword(Model model, HttpServletRequest request) {
		String idUser = request.getParameter("idUser"); // untuk update data, primary key wajib di panggil

		UserModel userModel = new UserModel();

		userModel = this.userService.searchId(idUser);

		model.addAttribute("userModel", userModel);

		String jsp = "user/resetPassword";
		return jsp;
	}
	// ============================================================================================//

	@RequestMapping(value = "user/updatePassword")
	public String userUpdatePassword(Model model, HttpServletRequest request) throws ParseException {
		String idUser = request.getParameter("idUser");

		String jsp;
		String password = request.getParameter("editPassword1");
		String rePassword = request.getParameter("editPassword2");

		UserModel userModel = new UserModel();

		userModel = this.userService.searchId(idUser);

		userModel.setPassword(password);

		if (password.equals(rePassword)) {
			this.userService.update(userModel);
			model.addAttribute("kondisi", "1");
			jsp = "user/home";
		} else {
			model.addAttribute("kondisi", 0);
			jsp = "user/add";
		}

		return jsp;
	}

	// =================== Remove ===================//
	@RequestMapping(value = "user/remove")
	public String userRemove(Model model, HttpServletRequest request) {
		String idUser = request.getParameter("idUser"); // untuk update data, primary key wajib di panggil

		UserModel userModel = new UserModel();

		userModel = this.userService.searchId(idUser);

		model.addAttribute("userModel", userModel);

		String jsp = "user/remove";
		return jsp;
	}

	// ============================================================================================//

	@RequestMapping(value = "user/deleteTemporary")
	public String userDeleteTemporary(HttpServletRequest request) {
		String idUser = request.getParameter("idUser");

		UserModel userModel = new UserModel();

		userModel = this.userService.searchId(idUser);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		Integer idRoleOld = userModel.getIdRole();
		String emailOld = userModel.getEmail();
		String usernameOld = userModel.getUsername();
		Boolean mobileFlagOld = userModel.getMobileFlag();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		userModel.setisDelete(true);

		// save audit trail untuk deleted
		Integer IdDeletedBy = this.userSearch().getIdUser();
		userModel.setIdDeletedBy(IdDeletedBy);
		userModel.setDeletedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("DELETE");
		auditModel.setJsonDelete("{ 'idRole' : '" + idRoleOld + "' , 'email' : '" + emailOld + "' , 'username' : '"
				+ usernameOld + "' , 'mobileFlag':'" + mobileFlagOld + "'}");
		auditModel.setIdCreatedBy(IdDeletedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.userService.update(userModel);

		String jsp = "user/home";
		return jsp;
	}

	// =================== Search by Username/ Email ===================//
	@RequestMapping(value = "user/search")
	public String userSearch(Model model, HttpServletRequest request) {
		String username = request.getParameter("search");
		String email = request.getParameter("search");

		List<UserModel> userModelList = new ArrayList<UserModel>();

		userModelList = this.userService.searchUsernameEmail(username, email);

		model.addAttribute("userModelList", userModelList);

		String jsp = "user/list";
		return jsp;
	}

	// =================== Reset ===================//
	@RequestMapping(value = "user/reset")
	public String userCancelCreate() {

		String jsp = "user/home";
		return jsp;
	}
}
