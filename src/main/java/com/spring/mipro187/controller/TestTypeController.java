package com.spring.mipro187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.AuditModel;
import com.spring.mipro187.model.TestTypeModel;
import com.spring.mipro187.service.AuditService;
import com.spring.mipro187.service.TestTypeService;

@Controller
public class TestTypeController extends AdminController {

	@Autowired
	private TestTypeService testTypeService;

	@Autowired
	private AuditService auditService;

	// =================== Home ===================//
	@RequestMapping(value = "testType")
	public String testType() {

		String jsp = "testType/home";
		return jsp;
	}

	// =================== List ===================//
	@RequestMapping(value = "testType/list")
	public String testTypeList(Model model) {

		List<TestTypeModel> testTypeModelList = new ArrayList<TestTypeModel>();

		testTypeModelList = this.testTypeService.searchAll();

		model.addAttribute("testTypeModelList", testTypeModelList);

		String jsp = "testType/list";
		return jsp;
	}

	// =================== Add ===================//
	@RequestMapping(value = "testType/add")
	public String testTypeAdd() {

		String jsp = "testType/add";
		return jsp;
	}

	// ============================================================================================//
	@RequestMapping(value = "testType/create")
	public String testTypeCreate(HttpServletRequest request) {
		String nama = request.getParameter("testTypeName");
		String notes = request.getParameter("notes");

		TestTypeModel testTypeModel = new TestTypeModel();

		testTypeModel.setNamaTestType(nama);
		testTypeModel.setNoteTestType(notes);

		testTypeModel.setIsDelete(false);
		testTypeModel.setTypeOfAnswer(0);

		Integer IdCreatedBy = this.userSearch().getIdUser();
		testTypeModel.setIdCreatedBy(IdCreatedBy);
		testTypeModel.setCreatedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("INSERT");
		auditModel.setJsonInsert("{ 'name' : '" + nama + "' , 'notes' : '" + notes + "' }");
		auditModel.setIdCreatedBy(IdCreatedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.testTypeService.create(testTypeModel);

		String jsp = "testType/home";
		return jsp;
	}

	// =================== Edit ===================//
	@RequestMapping(value = "testType/edit")
	public String testTypeEdit(Model model, HttpServletRequest request) {
		String idTestType = request.getParameter("idTestType");

		TestTypeModel testTypeModel = new TestTypeModel();

		testTypeModel = this.testTypeService.searchId(idTestType);

		model.addAttribute("testTypeModel", testTypeModel);

		String jsp = "testType/edit";
		return jsp;
	}

	// ============================================================================================//
	@RequestMapping(value = "testType/update")
	public String testTypeUpdate(HttpServletRequest request) {
		String idTestType = request.getParameter("idTestType");

		String name = request.getParameter("testTypeName");
		String notes = request.getParameter("notes");

		TestTypeModel testTypeModel = new TestTypeModel();

		testTypeModel = this.testTypeService.searchId(idTestType);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		String namaOld = testTypeModel.getNamaTestType();
		String notesOld = testTypeModel.getNoteTestType();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		testTypeModel.setNamaTestType(name);
		testTypeModel.setNoteTestType(notes);

		// save audit trail untuk updated
		Integer IdModifiedBy = this.userSearch().getIdUser();
		testTypeModel.setIdModifiedBy(IdModifiedBy);
		testTypeModel.setModifiedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("MODIFY");
		auditModel.setJsonBefore("{ 'name' : '" + namaOld + "' , 'notes' : '" + notesOld + "' }");
		auditModel.setJsonAfter("{ 'name' : '" + name + "' , 'notes' : '" + notes + "' }");
		auditModel.setIdCreatedBy(IdModifiedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.testTypeService.update(testTypeModel);

		String jsp = "testType/home";
		return jsp;
	}

	// =================== Remove Data ===================//
	@RequestMapping(value = "testType/remove")
	public String testTypeRemove(Model model, HttpServletRequest request) {
		String idTestType = request.getParameter("idTestType");

		TestTypeModel testTypeModel = new TestTypeModel();

		testTypeModel = this.testTypeService.searchId(idTestType);

		model.addAttribute("testTypeModel", testTypeModel);

		String jsp = "testType/remove";
		return jsp;
	}

	// ============================================================================================//
	@RequestMapping(value = "testType/deleteTemporary")
	public String testTypeDeleteTemporary(HttpServletRequest request) {
		String idTestType = request.getParameter("idTestType");

		TestTypeModel testTypeModel = new TestTypeModel();

		testTypeModel = this.testTypeService.searchId(idTestType);

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		String namaOld = testTypeModel.getNamaTestType();
		String notesOld = testTypeModel.getNoteTestType();
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		testTypeModel.setIsDelete(true);

		// save audit trail untuk deleted
		Integer IdDeletedBy = this.userSearch().getIdUser();
		testTypeModel.setIdDeletedBy(IdDeletedBy);
		testTypeModel.setDeletedOn(new Date());

		// ~~~~~~~~~~~~~~~~~~ Audit Log ~~~~~~~~~~~~~~~~~~
		AuditModel auditModel = new AuditModel();

		auditModel.setType("DELETE");
		auditModel.setJsonDelete("{ 'name' : '" + namaOld + "' , 'notes' : '" + notesOld + "' }");
		auditModel.setIdCreatedBy(IdDeletedBy);
		auditModel.setCreatedOn(new Date());

		this.auditService.create(auditModel);
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		this.testTypeService.update(testTypeModel);

		String jsp = "testType/home";
		return jsp;
	}

	// =================== Search by Name ===================//
	@RequestMapping(value = "testType/search")
	public String testTypeSearchNama(Model model, HttpServletRequest request) {
		String namaTestType = request.getParameter("searchNama");

		List<TestTypeModel> testTypeModelList = new ArrayList<TestTypeModel>();

		testTypeModelList = this.testTypeService.searchNamaLike(namaTestType);

		model.addAttribute("testTypeModelList", testTypeModelList);

		String jsp = "testType/list";
		return jsp;
	}

	// =================== Reset ===================//
	@RequestMapping(value = "testType/reset")
	public String testTypeCancelCreate() {

		String jsp = "testType/home";
		return jsp;
	}
}
