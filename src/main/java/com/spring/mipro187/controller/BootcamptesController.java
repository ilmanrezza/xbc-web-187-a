package com.spring.mipro187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mipro187.model.BootcamptesModel;
import com.spring.mipro187.service.BootcamptesService;

@Controller
public class BootcamptesController extends AdminController {

	@Autowired
	BootcamptesService bootcamptesService;
	
	@RequestMapping(value = "bootcamptes")
	public String bootcamptes() {
		String jsp = "bootcamptes/bootcamptes";
		return jsp;
	}
	
	@RequestMapping(value = "bootcamptes/add")
	public String bootcamptesAdd()
	{
		String jsp = "bootcamptes/add";
		return jsp;
	}
	
	@RequestMapping(value = "bootcamptes/create")
	public String bootcamptesCreate(HttpServletRequest request)
	{	//String id = request.getParameter(String.valueOf("id"));
		String name = request.getParameter("name");
		String notes = request.getParameter("note");
		
		//dapatkan data dari front end
		BootcamptesModel bootcamptesModel = new BootcamptesModel();
		//save
				Integer idCreatedBy = this.userSearch().getIdUser();
				bootcamptesModel.setIdCreatedBy(idCreatedBy);
				bootcamptesModel.setCreatedOn(new Date());
				
		//bootcamptesModel.setId(Integer.valueOf(id));
		bootcamptesModel.setName(name);
		bootcamptesModel.setNotes(notes);
		bootcamptesModel.setIsDelete(false);
		this.bootcamptesService.create(bootcamptesModel);
		
		
		String jsp = "bootcamptes/bootcamptes";
		return jsp;
	}
	
	
	
	@RequestMapping(value = "bootcamptes/reset")
	public String bootcamptesReset()
	{
		String jsp = "bootcamptes/bootcamptes";
		return jsp;
	}
	
	
	@RequestMapping(value ="bootcamptes/list")
	public String bootcamptesList(Model model) {
		List<BootcamptesModel> bootcamptesModel = new ArrayList<BootcamptesModel>();
		//Integer id = this.userSearch().getIdUser();
		bootcamptesModel = this.bootcamptesService.searchAll();
		model.addAttribute("bootcamptesModel", bootcamptesModel);
		
		String jsp = "bootcamptes/list";
		return jsp;
	}
	
	
	@RequestMapping(value="bootcamptes/edit")
	public String bootcamptesEdit(Model model, HttpServletRequest request) {
		String id = request.getParameter("id");
		BootcamptesModel bootcamptesModel = new BootcamptesModel();
		bootcamptesModel = this.bootcamptesService.searchId(Integer.valueOf(id));
		
		model.addAttribute("bootcamptesModel", bootcamptesModel); //backend ke frontend, taro di edit.
		String jsp = "bootcamptes/edit";
		return jsp;
	}
	
	@RequestMapping(value = "bootcamptes/update")
	public String bootcamptesUpdate(Model model, HttpServletRequest request)
	{	String id = request.getParameter("id");
		String name = request.getParameter("name");
		String notes = request.getParameter("note");
		
		BootcamptesModel bootcamptesModel = new BootcamptesModel();
		bootcamptesModel = this.bootcamptesService.searchId(Integer.valueOf(id));
		//save
			Integer idupdatedBy = this.userSearch().getIdUser();
			bootcamptesModel.setIdModifiedBy(idupdatedBy);
			bootcamptesModel.setModifiedOn(new Date());
		bootcamptesModel.setName(name);
		bootcamptesModel.setNotes(notes);
		bootcamptesModel.setIsDelete(false);
		this.bootcamptesService.update(bootcamptesModel);
		String jsp = "bootcamptes/bootcamptes";
		return jsp;
	}
	
	@RequestMapping(value="bootcamptes/search/nama")
	public String bootcamptesSearchNama(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");
		List<BootcamptesModel> bootcamptesModel = new ArrayList<BootcamptesModel>();
		bootcamptesModel = this.bootcamptesService.name(name);
		
		model.addAttribute("bootcamptesModel", bootcamptesModel);
		String jsp = "bootcamptes/list";
		return jsp;
	}
	
	@RequestMapping(value="bootcamptes/remove")
	public String bootcamptesRemove(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		BootcamptesModel bootcamptesModel = new BootcamptesModel();
		bootcamptesModel = this.bootcamptesService.searchId(Integer.valueOf(id));
		model.addAttribute("bootcamptesModel", bootcamptesModel);
		String jsp ="bootcamptes/remove";
		return jsp;
	}
	
	@RequestMapping(value="bootcamptes/delete")
	public String bootcamptesDelete(HttpServletRequest request) {
		String id = request.getParameter("id");
		BootcamptesModel bootcamptesModel = new BootcamptesModel();
		bootcamptesModel = this.bootcamptesService.searchId(Integer.valueOf(id));
		
		bootcamptesModel.setIsDelete(true);
		
		
		Integer iddeletedBy = this.userSearch().getIdUser();
		bootcamptesModel.setIdDeletedBy(iddeletedBy);
		bootcamptesModel.setDeletedOn(new Date());
		
		
		this.bootcamptesService.deleteTemporary(bootcamptesModel);
		String jsp = "bootcamptes/bootcamptes";
		return jsp;
	}
}
