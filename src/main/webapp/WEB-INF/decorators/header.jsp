<nav class = "navbar-fixed-top">
<header class="main-header">

    <!-- Logo -->
    <a href="/mipro187/index/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>HOME</b></span>
    </a>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      
      <div>
      	<ul class="nav navbar-nav">
      		<li>
      			<a>
      				Hi, ${username}
      			</a>
      		</li>
      	</ul>
      </div>

	  <div class="navbar-custom-menu">
	  	<div class="pull-right">
        	<form method="post" action="<c:url value="/j_spring_security_logout" />">
            	<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" /><br />
               	<button type="submit" class="btn btn-default btn-sm">Sign Out</button>
            </form> 
 	    </div>
      </div>
      
    </nav>
  </header>
  </nav>