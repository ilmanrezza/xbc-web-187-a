
<nav class="navbar" style="height: 100%; position: fixed;"
	role="navigation">
	<aside class="main-sidebar">

		<section class="sidebar">
			<ul class="sidebar-menu">
				<li><a href="#"><i class="fa fa-bars"></i><span>Master</span><span
						class="pull-right-container"> </span></a>
					<ul class="treeview-menu">

						<li><a href="${contextName}/biodata.html" class="menu-item"><i
								class="glyphicon glyphicon-paperclip"></i>Biodata</a></li>
						<li><a href="${contextName}/testType.html" class="menu-item"><i
								class="glyphicon glyphicon-paperclip"></i>Test Type</a></li>
						<li><a href="${contextName}/office.html" class="menu-item"><i
								class="glyphicon glyphicon-paperclip"></i>Office</a></li>
						<li><a href="${contextName}/bootcamptes.html"
							class="menu-item"><i class="glyphicon glyphicon-paperclip"></i>Bootcamp
								Test</a></li>
						<li><a href="${contextName}/user.html" class="menu-item"><i
								class="glyphicon glyphicon-paperclip"></i>User</a></li>

					</ul></li>

				<li><a href="#"><i class="fa fa-bars"></i> <span>Transaksi</span><span
						class="pull-right"></span></a>
					<ul class="treeview-menu">
						<li><a href="${contextName}/monitoring.html"
							class="menu-item"> <i class="glyphicon glyphicon-eye-open"></i>Idle
								Monitoring
						</a></li>
						<li><a href="${contextName}/assignment.html"
							class="menu-item"> <i class="glyphicon glyphicon-eye-open"></i>Assignment
						</a></li>
					</ul></li>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
</nav>