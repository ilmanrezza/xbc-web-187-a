<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${biodataModelList}" var="biodataModel" varStatus="number">
	<tr>
		
		<td>${biodataModel.name}</td>
		<td>${biodataModel.majors}</td>
		<td>${biodataModel.gpa}</td>
		<td>
			<div class="dropdown">
				<button type="button" class="btn btn-warning dropdown-toggle"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanden="false">=</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<button class="btn btn-info btn-block" value="${biodataModel.id}"
						id="btn-edit">Edit</button>
					<button class="btn btn-danger btn-block"
						value="${biodataModel.id}" id="btn-delete">Hapus</button>
				</div>

			</div>

		</td>
	</tr>
</c:forEach>