<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
	box-sizing: border-box;
}
/* Create two equals coll */
.column {
	float: left;
	width: 50%;
	padding: 10px;
	height: 225px;
}

/* Clear floats after the columns */
.row:after {
	content: " ";
	display: tae;
	clear: both;
}

.textarea {
	padding: 10px;
}
</style>
<div class="form-horizontal">
	<h1>Edit Biodata</h1>
	
	<form action="#" method="get" id="form-biodata-edit">
		<input type="hidden" id="id" name="id" value="${biodataModel.id}" />
		<div class="row">
			<div class="column">

				<p>
					<input type="text" id="name" name="name" placeholder="Name"
						size="40" value="${biodataModel.name}" />
				</p>

				<p>
					<input type="text" id="lastEducation" name="lastEducation"
						placeholder="Last Education" size="40"
						value="${biodataModel.lastEducation}" />
				</p>

				<p>
					<input type="text" id="educationalLevel" name="educationalLevel"
						placeholder="Education Level" size="40"
						value="${biodataModel.educationalLevel}" />
				</p>

				<p>
					<input type="text" id="graduationYear" name="graduationYear"
						placeholder="Graduation Year" size="40"
						value="${biodataModel.graduationYear}" />
				</p>

				<p>
					<input type="text" id="majors" name="majors" placeholder="Majors"
						size="40" value="${biodataModel.majors}" />
				</p>

				<p>
					<input type="text" id="gpa" name="gpa" placeholder="Gpa" size="40"
						value="${biodataModel.gpa}" />
				</p>

			</div>
			<div class="column">
				<p>
					Gender <input type="radio" id="gender" name="gender" value="M" />M
					<input type="radio" id="gender" name="gender" value="L" />L
				</p>

				<p>
					<select id="idBootcampTest" name="idBootcampTest">
						<c:forEach items="${bootcamptesModelList}" var="bootcamptesModel"
							varStatus="number">
							<option value="${bootcamptesModel.id}">${bootcamptesModel.name}</option>
						</c:forEach>
					</select>
				</p>

				<p>
					<input type="text" id="iq" name="iq" placeholder="IQ" size="3"
						value="${biodataModel.iq}" /> <input type="text" id="du"
						name="du" placeholder="DU" size="3" value="${biodataModel.du}" />
					<input type="text" id="nestedLogic" name="nestedLogic"
						placeholder="NL" size="3" value="${biodataModel.nestedLogic}" />
					<input type="text" id="joinTable" name="joinTable" placeholder="JT"
						size="3" value="${biodataModel.joinTable}" />
				</p>

				<p>
					<input type="text" id="arithmetic" name="arithmetic"
						placeholder="Arithmetic" size="40"
						value="${biodataModel.arithmetic}" />
				</p>

				<p>
					<input type="text" id="tro" name="tro" placeholder="TRO" size="40"
						value="${biodataModel.tro}" />
				</p>

				<p>
					<input type="text" id="interviewer" name="interviewer"
						placeholder="Interviewer" size="40"
						value="${biodataModel.interviewer}" />
				</p>
			</div>
			<div class="textarea">
				<p>
					<textarea cols="90" rows="3" id="notes" name="notes"
						placeholder="Notes" value="${biodataModel.notes}">${biodataModel.notes}</textarea>
				</p>
			</div>
		</div>
		<br>
		<button type="submit" onclick="validasi();">Simpan</button>
	</form>
</div>
<script type="text/javascript">
	function validasi() {
		var name = document.getElementById("name");
		if (name.value == "") {
			name.style.borderColor = "red";
		} else {
			name.style.borderColor = "green";
		}

		var lastEducation = document.getElementById("lastEducation");
		if (lastEducation.value == "") {
			lastEducation.style.borderColor = "red";
		} else {
			lastEducation.style.borderColor = "green";
		}

		var EducationLevel = document.getElementById("EducationLevel");
		if (EducationLevel.value == "") {
			EducationLevel.style.borderColor = "red";
		} else {
			EducationLevel.style.borderColor = "green";
		}

		var GraduationYear = document.getElementById("GraduationYear");
		if (GraduationYear.value == "") {
			GraduationYear.style.borderColor = "red";
		} else {
			GraduationYear.style.borderColor = "green";
		}

		var majors = document.getElementById("majors");
		if (majors.value == "") {
			majors.style.borderColor = "red";
		} else {
			majors.style.borderColor = "green";
		}

		var gpa = document.getElementById("gpa");
		if (gpa.value == "") {
			gpa.style.borderColor = "red";
		} else {
			gpa.style.borderColor = "green";
		}
	}
</script>