<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

	<h1 style="text-align: center;">Hapus Biodata</h1>
	<div>
	<form action="#" method="get" id="form-biodata-delete">

		<input type="hidden" id="id" name="id"
			value="${biodataModel.id}" />

		<div class="row">
			<div class="col-sm-10">
				<button id="btn-cancel" class="btn btn-danger" type="button">CANCEL</button>
			</div>
			<div class="col-sm-2">
				<button class="btn btn-success" type="submit">DELETE</button>
			</div>
		</div>
	</form>
</div>
