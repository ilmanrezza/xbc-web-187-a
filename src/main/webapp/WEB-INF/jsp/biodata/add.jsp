<div class="form-horizontal">
	<h1>Tambah Biodata</h1>
	<form action="#" method="get" id="form-biodata-add">
	<!-- <input type="hidden" id="id" name="id" /> -->
		<table>
			<tr>
				<td><input type="text" id="name" name="name"
					placeholder="Name" /></td>
			</tr>
			<tr>
				<td><input type="text" id="lastEducation" name="lastEducation"
					placeholder="Last Education" /></td>
			</tr>
			<tr>
				<td><input type="text" id="educationalLevel" name="educationalLevel"
					placeholder="Educational Level" /></td>
			</tr>
			<tr>
				<td><input type="text" id="graduationYear" name="graduationYear"
					placeholder="Graduation Year" /></td>
			</tr>
			<tr>
				<td><input type="text" id="majors" name="majors"
					placeholder="Majors" /></td>
			</tr>
			<tr>
				<td><input type="text" id="gpa" name="gpa"
					placeholder="Gpa" /></td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button>
	</form>
</div>
<script type="text/javascript">
	function validasi() {
		var name = document.getElementById("name");
		if (name.value == "") {
			name.style.borderColor = "red";
		} else {
			name.style.borderColor = "green";
		}

		var lastEducation = document.getElementById("lastEducation");
		if (lastEducation.value == "") {
			lastEducation.style.borderColor = "red";
		} else {
			lastEducation.style.borderColor = "green";
		}

		var educationalLevel = document.getElementById("educationalLevel");
		if (educationalLevel.value == "") {
			educationalLevel.style.borderColor = "red";
		} else {
			educationalLevel.style.borderColor = "green";
		}

		var graduationYear = document.getElementById("graduationYear");
		if (graduationYear.value == "") {
			graduationYear.style.borderColor = "red";
		} else {
			graduationYear.style.borderColor = "green";
		}

		var majors = document.getElementById("majors");
		if (majors.value == "") {
			majors.style.borderColor = "red";
		} else {
			majors.style.borderColor = "green";
		}

		var gpa = document.getElementById("gpa");
		if (gpa.value == "") {
			gpa.style.borderColor = "red";
		} else {
			gpa.style.borderColor = "green";
		}
	}
</script>