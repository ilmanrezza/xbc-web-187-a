<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Biodata</h1>
	<div>
		<table class="table" id="tbl-biodata">
		<tr>
				<td><form action="#" method="get" id="form-biodata-search">
				<input type="text" id="nameOrMajors" name="nameOrMajors" placeholder="Search by name/majors" style="height: 30px; width: 300px;" size="20"/>
				<button type="submit" class="btn btn-warning btn-sm" style="height: 30px;">O</button>
				</form>
				</td>
				<td></td>
				<td><button type="button" class="btn btn-warning" id="btn-add">+</button></td>
				<td></td>
			</tr>
			<tr>
				<td>NAME</td>
				<td>MAJORS</td>
				<td>GPA</td>
			</tr>
			<tbody id="list-biodata">
			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Biodata</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	loadListBiodata();

	function loadListBiodata() {
		$.ajax({
			url : 'biodata/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-biodata').html(result)
			}
		});
	}

	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'biodata/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-biodata-add', function() {// #modal-input, buat wadah popupnya
			$.ajax({
				url : 'biodata/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data biodata telah ditambah!");
					loadListBiodata();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah

		//fungsi ajax pop up edit
		$('#list-biodata').on('click', '#btn-edit', function() {
			var id = $(this).val();
			$.ajax({
				url : 'biodata/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					id : id
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir dari funsi ajax pop up

		//awal fungsi ajax update
		$('#modal-input').on('submit', '#form-biodata-edit', function() {
			$.ajax({
				url : 'biodata/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data berhasil di update");
					loadListBiodata();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//Awal fungsi popup delete
		$('#list-biodata').on('click', '#btn-delete', function() {
			var id = $(this).val();
			$.ajax({
				url : 'biodata/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					id : id
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//Akhir fungsi popup delete
		
		//Awal fungsi delete json
		$('#modal-input').on('submit', '#form-biodata-delete', function(){
			$.ajax({
				url : 'biodata/delete.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result){
					$('#modal-input').modal('hide');
					alert('data telah berhasil dihapus')
					loadListBiodata();
				}
			});
			return false;
		});
		//Akhir fungsi delete json
		
		//fungsi search by name/majors
		$('#form-biodata-search').on('submit', function() {
			$.ajax({
				url : 'biodata/search',
				type : 'get',
				dataType : 'html',
				data : $(this).serialize(),
				success : function(result) {
					alert("data dicari berdasarkan nama dan majors");
					$('#list-biodata').html(result);
				}
			});
			return false;
		});
	});
</script>