<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">CREATE ASSIGNMENT</h1>
	</div>

	<form action="#" method="get" id="form-assignment-add">
		<div class="form-group">
			<div class="col-md-12">
				<select id="idBiodata" name="idBiodata" class="form-control"
					required>
					<option value="">-Choose Biodata Name-</option>
					<c:forEach items="${biodataModelList}" var="biodataModel"
						varStatus="number">
						<option value="${biodataModel.id}">${biodataModel.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="title" name="title" class="form-control"
					placeholder="Title" size="25" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="startDate" name="startDate"
					class="form-control" placeholder="Start Date"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="endDate" name="endDate" class="form-control"
					placeholder="End Date" onfocus="(type='date')"
					onblur="(type='text')" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="3" cols="27" id="description" name="description"
					class="form-control" placeholder="Description..."></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			onclick="validasi()" style="float: right; width: 100px;">Save</button>
	</form>
</div>

<script type="text/javascript">
	function validasi() {
		var endDate = document.getElementById("endDate");
		if (endDate.value < startDate.value) {
			endDate.style.borderColor = "red";
		} else {
			endDate.style.borderColor = "gray";
		}

	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>