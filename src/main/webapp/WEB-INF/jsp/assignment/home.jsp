
<div class="box-header with-border panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<h1>ASSIGNMENT</h1>

	<div class="box box-primary box-body">
		<table class="table" id="tbl-assignment">
			<thead>
				<tr>
					<td><form action="#" method="get" id="form-assignment-search"
							style="margin: auto; max-width: 700px">
							<input type="text" class="col-xs-6" id="searchDate"
								name="searchDate" placeholder="search by date ('dd-MMM-yy')" />
							<button style="background-color: white;" type="submit">O</button>
						</form></td>
					<td></td>
					<td></td>
					<td>
						<button type="button"
							class="btn btn-primary btn-md glyphicon glyphicon-plus btn-sm"
							id="btn-add"></button>
					</td>
					<td></td>
				</tr>
			</thead>
		</table>
		<table class="table table-striped">
			<thead>
				<tr style="font-weight: bold;">
					<td>Name</td>
					<td>Start Date</td>
					<td>End Date</td>
					<td>#</td>
					<td></td>
				</tr>
			</thead>

			<tbody id="list-assignment">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog" style="width: 500px">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListAssignment();

	function loadListAssignment() {
		$.ajax({
			url : 'assignment/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-assignment').html(result);
			}
		});
	}

	$(document).ready(function() {

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup add
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'assignment/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup add

		//fungsi ajax untuk create
		$('#modal-input').on('submit', '#form-assignment-add', function() {
			$.ajax({
				url : 'assignment/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					if (result.kondisi == 1) {
						$('#modal-input').modal('hide');
						alert("Data has been added!");
					} else {
						alert("End date must after Start date!");
					}
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create

		//fungsi ajax untuk cancel create
		$('#modal-input').on('reset', '#form-assignment-add', function() {

			$.ajax({
				url : 'assignment/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel create

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup edit
		$('#list-assignment').on('click', '#btn-edit', function() {
			var idAssignment = $(this).val();
			$.ajax({
				url : 'assignment/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					idAssignment : idAssignment
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-assignment-edit', function() {
			$.ajax({
				url : 'assignment/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					if (result.kondisi == 1) {
						$('#modal-input').modal('hide');
						alert("Data has been edited!");
					} else {
						alert("End date must after Start date!");
					}
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk cancel update
		$('#modal-input').on('reset', '#form-assignment-edit', function() {

			$.ajax({
				url : 'assignment/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel update

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup editMark
		$('#list-assignment').on('click', '#btn-mark', function() {
			var idAssignment = $(this).val();
			$.ajax({
				url : 'assignment/mark.html',
				type : 'get',
				dataType : 'html',
				data : {
					idAssignment : idAssignment
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup editMark

		//fungsi ajax untuk updateMark
		$('#modal-input').on('submit', '#form-assignment-mark', function() {
			$.ajax({
				url : 'assignment/updateMark.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Marked as done!");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk updateMark

		//fungsi ajax untuk cancel updateMark
		$('#modal-input').on('reset', '#form-assignment-mark', function() {

			$.ajax({
				url : 'assignment/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel updateMark

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup remove
		$('#list-assignment').on('click', '#btn-remove', function() {
			var idAssignment = $(this).val();
			$.ajax({
				url : 'assignment/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					idAssignment : idAssignment
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup remove

		//fungsi ajax untuk update isDelete
		$('#modal-input').on('submit', '#form-assignment-remove', function() {

			$.ajax({
				url : 'assignment/deleteTemporary.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been erased!");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update isDelete

		//fungsi ajax untuk cancel delete
		$('#modal-input').on('reset', '#form-assignment-remove', function() {

			$.ajax({
				url : 'assignment/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel delete

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup hold
		$('#list-assignment').on('click', '#btn-hold', function() {
			var idAssignment = $(this).val();
			$.ajax({
				url : 'assignment/hold.html',
				type : 'get',
				dataType : 'html',
				data : {
					idAssignment : idAssignment
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hold

		//fungsi ajax untuk update isHold
		$('#modal-input').on('submit', '#form-assignment-hold', function() {

			$.ajax({
				url : 'assignment/isHold.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been holded!");
					loadListAssignment();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update isHold

		//fungsi ajax untuk cancel hold
		$('#modal-input').on('reset', '#form-assignment-hold', function() {

			$.ajax({
				url : 'assignment/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel hold

		// ------------------------------------------------------------ //

		//fungsi ajax untuk search date
		$('#form-assignment-search').on('submit', function() {
			$.ajax({
				url : 'assignment/search.html',
				type : 'get',
				dataType : 'html',
				data : $(this).serialize(),
				success : function(result) {
					alert("Data by date has been found!");
					$('#list-assignment').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search date

	});
	//akhir document ready
</script>