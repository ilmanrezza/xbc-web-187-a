<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${assignmentModelList}" var="assignmentModel"
	varStatus="number">
	<tr>
		<td>${assignmentModel.biodataModel.name}</td>
		<td>${assignmentModel.startDate}</td>
		<td>${assignmentModel.endDate}</td>
		<td>
			<div class="dropdown">
				<button type="button"
					class="btn btn-primary dropdown-toggle fa fa-bars"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanden="false"></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<button style="background-color: white; border: 0px;"
						value="${assignmentModel.idAssignment}" id="btn-edit">Edit</button>
					<br />
					<button style="background-color: white; border: 0px;"
						value="${assignmentModel.idAssignment}" id="btn-remove">Delete</button>
					<br />
					<button style="background-color: white; border: 0px;"
						value="${assignmentModel.idAssignment}" id="btn-hold">Hold</button>
					<br />
					<button style="background-color: white; border: 0px;"
						value="${assignmentModel.idAssignment}" id="btn-mark">Mark as done</button>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>