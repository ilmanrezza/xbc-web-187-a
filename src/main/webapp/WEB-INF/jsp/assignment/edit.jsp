<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">EDIT DATA
			${assignmentModel.biodataModel.name}</h1>
	</div>

	<form action="#" method="get" id="form-assignment-edit">
		<input type="hidden" class="form-control"
			value="${assignmentModel.idAssignment}" name="idAssignment" />

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="editTitle" name="editTitle"
					class="form-control" placeholder="${assignmentModel.title}"
					size="25" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="editStartDate" name="editStartDate"
					class="form-control" placeholder="${assignmentModel.startDate}"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="editEndDate" name="editEndDate"
					class="form-control" placeholder="${assignmentModel.endDate}"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="3" cols="27" id="editDescription"
					name="editDescription" class="form-control"
					placeholder="${assignmentModel.description}"></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			onclick="validasi()" style="float: right; width: 100px;">Save</button>
	</form>
</div>

<script type="text/javascript">
	function validasi() {
		var editEndDate = document.getElementById("editEndDate");
		if (editEndDate.value < editStartDate.value) {
			editEndDate.style.borderColor = "red";
		} else {
			editEndDate.style.borderColor = "gray";
		}

	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>