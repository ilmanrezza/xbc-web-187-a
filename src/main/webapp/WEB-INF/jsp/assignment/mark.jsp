<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">MARK AS DONE</h1>
	</div>

	<form action="#" method="get" id="form-assignment-mark">
		<input type="hidden" class="form-control"
			value="${assignmentModel.idAssignment}" name="idAssignment" />

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="realizationDate" name="realizationDate"
					class="form-control" placeholder="Realization Date"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="5" cols="25" name="notes" class="form-control"
					placeholder="Notes"></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;">Save</button>

	</form>
</div>

<script type="text/javascript">
	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>