<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${roomModelList}" var="roomModel"
	varStatus="number">
	<tr>
		<td>${roomModel.codeRoom}</td>
		<td>${roomModel.nameRoom}</td>
		<td>${roomModel.capacity}</td>
		<td>
			<div class="dropdown">
				<button type="button"
					class="btn btn-primary dropdown-toggle fa fa-bars"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanden="false"></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<button style="background-color: white; border: 0px;"
						value="${roomModel.idRoom}" id="btn-editRoom">Edit</button>
					<br />
					<button style="background-color: white; border: 0px;"
						value="${roomModel.idRoom}" id="btn-removeRoom">Delete</button>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>