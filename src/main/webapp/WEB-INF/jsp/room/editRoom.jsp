<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">EDIT ROOM</h1>
	</div>

	<form action="#" method="get" id="form-room-edit">
		<input type="hidden" value="${roomModel.idRoom}" name="idRoom" />

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="codeRoom" class="form-control"
					name="codeRoom" placeholder="codeRoom" size="25" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="nameRoom" class="form-control"
					name="nameRoom" placeholder="nameRoom" size="25" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="capacity" class="form-control"
					name="capacity" placeholder="capacity" size="25" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<h5>Any projector?</h5>
			</div>
			<div class="col-md-6">
				<label class="radio-inline"> <input type="radio"
					name="projector" id="projector" value="yes" checked>Yes
				</label> <label class="radio-inline"> <input type="radio"
					name="projector" id="projector" value="no">No
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="3" cols="20" id="description" class="form-control"
					name="descriptionRoom" placeholder="Description..."></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;">Save</button>

	</form>
</div>

<script type="text/javascript">
	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>