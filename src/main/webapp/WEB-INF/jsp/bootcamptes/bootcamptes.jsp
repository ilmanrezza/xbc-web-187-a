<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Bootcamp Test type</h1>

	<div>
		<table class="table" id="tbl-bootcamptes">
			<tr>
				<td><form action="#" method="get" id="form-bootcamptes-search-name">
						<input type="text" id="name" name="name"
							placeholder="Search by name" style="height: 30px; width: 300px;" />
						   <button type="submit" class="btn btn-warning">
							 <span class="glyphicon glyphicon-search"></span> Search
							</button>
					</form></td>
				<td></td>
				<td><button type="button"  id="btn-add" style="border:5; background-color : orange;">
				<span class="glyphicon glyphicon-plus"></span>
				</button></td>
				<td></td>

			</tr>
			<tr>
				<td style="font-weight: bold;">NAME</td>
				<td style="font-weight: bold;">CREATED BY</td>
				<td>
				 <span class="glyphicon glyphicon-th-list"></span>
				</td>
			</tr>
			<tbody id="list-bootcamptes">

			</tbody>
		</table>
	</div>

</div>
<!-- syntax popup Add-->
<div id="modal-input-add" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Bootcamp Test Type</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			</div>
		</div>
	</div>
</div>



<!-- syntax popup edit -->
<div id="modal-input-edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Bootcamp Test Type</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			</div>
		</div>
	</div>
</div>



<!-- syntax popup remove -->
<div id="modal-input-remove" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Delete</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListBootcamptes();

	function loadListBootcamptes() {
		$.ajax({
			url : 'bootcamptes/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-bootcamptes').html(result);
			}
		});
	}

	//fungsi ajax untuk popup tambah
	$('#btn-add').on('click', function() {
		$.ajax({
			url : 'bootcamptes/add.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input-add').find('.modal-body').html(result);
				$('#modal-input-add').modal('show');
			}
		});
	});
	//akhir fungsi ajax untuk popup tambah
	
		//fungsi ajax untuk create tambah
		$('#modal-input-add').on('submit', '#form-bootcamptes-add', function() {
			$.ajax({
				url : 'bootcamptes/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					
					$('#modal-input-add').modal('hide');
					alert("data telah ditambah!");
					loadListBootcamptes();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		
		

		//fungsi ajax untuk search nama
		$('#form-bootcamptes-search-name').on('submit',function(){
			$.ajax({
				url:'bootcamptes/search/nama.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama!");
					$('#list-bootcamptes').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search nama
		
		
		
		//fungsi ajax untuk popup edit
		$('#list-bootcamptes').on('click','#btn-edit',function(){
			var id = $(this).val();
			$.ajax({
				url : 'bootcamptes/edit.html',
				type : 'get',
				dataType : 'html',
				data:{id:id},
				success : function(result) {
					$('#modal-input-edit').find('.modal-body').html(result);
					$('#modal-input-edit').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input-edit').on('submit','#form-bootcamptes-edit',function(){
			
			$.ajax({
				url:'bootcamptes/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-edit').modal('hide');
					alert("data telah diubah!");
					loadListBootcamptes();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
		
		//fungsi ajax untuk hapus
		$('#list-bootcamptes').on('click','#btn-delete',function(){
			var id = $(this).val();
			$.ajax({
				url:'bootcamptes/remove.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-input-remove').find('.modal-body').html(result);
					$('#modal-input-remove').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input-remove').on('submit','#form-bootcamptes-delete',function(){
			
			$.ajax({
				url:'bootcamptes/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-remove').modal('hide');
					alert("data telah dihapus!");
					loadListBootcamptes();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		
</script>
