<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">ADD USER</h1>
	</div>

	<form action="#" method="get" id="form-user-add">
		<div class="form-group">
			<div class="col-md-12">
				<select id="idRole" name="idRole" class="form-control" required>
					<option value="">-Choose Role-</option>
					<c:forEach items="${roleModelList}" var="roleModel"
						varStatus="number">
						<option value="${roleModel.idRole}">${roleModel.namaRole}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="email" name="email" class="form-control"
					placeholder="Email" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="username" name="username"
					class="form-control" placeholder="Username" size="25" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="password" id="password" name="password"
					class="form-control" placeholder="Password" size="25" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="password" id="rePassword" name="rePassword"
					class="form-control" placeholder="Re-type Password" size="25"
					required />
			</div>
		</div>

		<br />
		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;" onclick="validasi();">Save</button>

	</form>
</div>

<script type="text/javascript">
	function validasi() {
		var rePassword = document.getElementById("rePassword");
		if (rePassword.value != password.value) {
			rePassword.style.borderColor = "red";
		} else {
			rePassword.style.borderColor = "gray";
		}
	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>