<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">EDIT USER</h1>
	</div>

	<form action="#" method="get" id="form-user-edit">
		<input type="hidden" value="${userModel.idUser}" name="idUser" />

		<div class="form-group">
			<div class="col-md-12">
				<select id="editIdRole" name="editIdRole" class="form-control"
					required>
					<option value="">--${userModel.roleModel.namaRole}--</option>
					<c:forEach items="${roleModelList}" var="roleModel"
						varStatus="number">
						<option value="${roleModel.idRole}">${roleModel.namaRole}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="editEmail" class="form-control"
					name="editEmail" placeholder="${userModel.email}" size="25"
					required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="editUsername" class="form-control"
					name="editUsername" placeholder="${userModel.username}" size="25"
					required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<h5>Mobile Flag</h5>
			</div>
			<div class="col-md-6">
				<label class="radio-inline"> <input type="radio"
					name="mobileFlag" id="mobileFlag" value="yes">True
				</label> <label class="radio-inline"> <input type="radio"
					name="mobileFlag" id="mobileFlag" value="no" checked>False
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="3" cols="20" id="mobileToken" class="form-control"
					name="mobileToken" placeholder="Mobile Token"></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;">Save</button>

	</form>
</div>

<script type="text/javascript">
	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>