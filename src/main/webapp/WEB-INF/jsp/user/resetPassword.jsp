<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">RESET PASSWORD</h1>
	</div>

	<form action="#" method="get" id="form-user-reset">
		<input type="hidden" value="${userModel.idUser}" name="idUser" />

		<div class="form-group">
			<div class="col-md-12">
				<input type="password" id="editPassword1" class="form-control"
					name="editPassword1" placeholder="Password" size="25" required />
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="password" id="editPassword2" class="form-control"
					name="editPassword2" placeholder="Re-type password" size="25"
					required />
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;" onclick="validasi();">Save</button>

	</form>
</div>

<script type="text/javascript">
	function validasi() {
		var editPassword2 = document.getElementById("editPassword2");
		if (editPassword2.value != editPassword1.value) {
			editPassword2.style.borderColor = "red";
		} else {
			editPassword2.style.borderColor = "gray";
		}
	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>