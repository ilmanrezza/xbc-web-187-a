
<div class="box-header with-border panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<h1>USER</h1>

	<div class="box box-primary box-body">
		<table class="table" id="tbl-user">
			<thead>
				<tr>
					<td><form action="#" method="get" id="form-user-search"
							style="margin: auto; max-width: 700px">
							<input type="text" class="col-xs-6" id="searchNama"
								name="searchNama" placeholder="search by username/ email" />
							<button style="background-color: white;" type="submit">O</button>
						</form></td>
					<td></td>
					<td>
						<button type="button"
							class="btn btn-primary btn-md glyphicon glyphicon-plus btn-sm"
							id="btn-add"></button>
					</td>
					<td></td>
				</tr>
			</thead>
		</table>
		<table class="table table-striped">
			<thead>
				<tr style="font-weight: bold;">
					<td>Username</td>
					<td>Role</td>
					<td>Email</td>
					<td>#</td>
				</tr>
			</thead>

			<tbody id="list-user">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog" style="width: 350px">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListUser();

	function loadListUser() {
		$.ajax({
			url : 'user/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-user').html(result);
			}
		});
	}

	$(document).ready(function() {

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup add
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'user/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup add

		//fungsi ajax untuk create
		$('#modal-input').on('submit', '#form-user-add', function() {
			$.ajax({
				url : 'user/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					if (result.kondisi == "1") {
						$('#modal-input').modal('hide');
						alert("Data has been added!");
					} else {
						alert("Password did not match!");
					}

					loadListUser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create

		//fungsi ajax untuk cancel create
		$('#modal-input').on('reset', '#form-user-add', function() {

			$.ajax({
				url : 'user/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel create

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup edit
		$('#list-user').on('click', '#btn-edit', function() {
			var idUser = $(this).val();
			$.ajax({
				url : 'user/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					idUser : idUser
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-user-edit', function() {
			$.ajax({
				url : 'user/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been changed!");
					loadListUser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk cancel update
		$('#modal-input').on('reset', '#form-user-edit', function() {

			$.ajax({
				url : 'user/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel update

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup edit password
		$('#list-user').on('click', '#btn-resetPassword', function() {
			var idUser = $(this).val();
			$.ajax({
				url : 'user/resetPassword.html',
				type : 'get',
				dataType : 'html',
				data : {
					idUser : idUser
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit password

		//fungsi ajax untuk update password
		$('#modal-input').on('submit', '#form-user-reset', function() {
			$.ajax({
				url : 'user/updatePassword.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					if (result.kondisi == "1") {
						$('#modal-input').modal('hide');
						alert("Password successfully changed!");
					} else {
						alert("Password did not match!");
					}
					
					loadListUser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update password

		//fungsi ajax untuk cancel update password
		$('#modal-input').on('reset', '#form-user-reset', function() {

			$.ajax({
				url : 'user/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel update password

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup remove
		$('#list-user').on('click', '#btn-remove', function() {
			var idUser = $(this).val();
			$.ajax({
				url : 'user/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					idUser : idUser
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup remove

		//fungsi ajax untuk update isDelete
		$('#modal-input').on('submit', '#form-user-remove', function() {

			$.ajax({
				url : 'user/deleteTemporary.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been erased!");
					loadListUser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update isDelete

		//fungsi ajax untuk cancel delete
		$('#modal-input').on('reset', '#form-user-remove', function() {

			$.ajax({
				url : 'user/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel delete

		// ------------------------------------------------------------ //

		//fungsi ajax untuk search nama
		$('#form-user-search').on('submit', function() {
			$.ajax({
				url : 'user/search.html',
				type : 'get',
				dataType : 'html',
				data : $(this).serialize(),
				success : function(result) {
					alert("Data by name has been found!");
					$('#list-user').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search nama

	});
	//akhir document ready
</script>