<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal" align="center">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">DELETE USER</h1>
	</div>

	<form action="#" method="get" id="form-user-remove">
		<input type="hidden" value="${userModel.idUser}"
			name="idUser" />

		<div class="form-group">
			<div class="col-md-12">
				<label style="align-content: center;">Are you sure to delete
					this data?</label>
			</div>
		</div>

		<button type="reset" class="btn btn-success btn-md"
			style="width: 100px;">No</button>
		<button type="submit" class="btn btn-danger btn-md"
			style="width: 100px;">Yes</button>

	</form>
</div>

<script type="text/javascript">
	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>