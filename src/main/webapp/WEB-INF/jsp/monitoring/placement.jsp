<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">INPUT PLACEMENT</h1>
	</div>

	<form action="#" method="get" id="form-monitoring-placement">
		<input type="hidden" class="form-control"
			value="${monitoringModel.idMonitoring}" name="idMonitoring" /> <input
			type="hidden" class="form-control" id="idleDate"
			value="${monitoringModel.idleDate}" name="idleDate" />

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="placementDate" name="placementDate"
					class="form-control" placeholder="Placement Date"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="placementAt" name="placementAt" required
					class="form-control" placeholder="Placement at" size="25" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="5" cols="25" name="notes" class="form-control"
					placeholder="Notes"></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button onclick="validasi();" type="submit"
			class="btn btn-success btn-md" style="float: right; width: 100px;">Save</button>

	</form>
</div>

<script type="text/javascript">
	function validasi() {
		var placementDate = document.getElementById("placementDate");
		if (placementDate.value < idleDate.value) {
			placementDate.style.borderColor = "red";
		} else {
			placementDate.style.borderColor = "gray";
		}

	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>