<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">EDIT DATA
			${monitoringModel.biodataModel.name}</h1>
	</div>

	<form action="#" method="get" id="form-monitoring-edit">
		<input type="hidden" class="form-control"
			value="${monitoringModel.idMonitoring}" name="idMonitoring" /> <input
			type="hidden" class="form-control" id="idleDate"
			value="${monitoringModel.idleDate}" name="idleDate" />

		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #adccff">
				<h3 class="panel-title">Idle Date</h3>
			</div>
			<div class="panel-body">
				<input type="text" id="editIdleDate" name="editIdleDate"
					class="form-control" placeholder="${monitoringModel.idleDate}"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #adccff">
				<h3 class="panel-title">Last Project</h3>
			</div>
			<div class="panel-body">
				<input type="text" id="editLastProject" class="form-control"
					name="editLastProject" placeholder="${monitoringModel.lastProject}"
					size="25" required />
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #adccff">
				<h3 class="panel-title">Idle Reason</h3>
			</div>
			<div class="panel-body">
				<input type="text" id="editIdleReason" class="form-control"
					name="editIdleReason" placeholder="${monitoringModel.idleReason}"
					size="25" required />
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #adccff">
				<h3 class="panel-title">Placement Date</h3>
			</div>
			<div class="panel-body">
				<input type="text" id="editPlacementDate" class="form-control"
					name="editPlacementDate"
					placeholder="${monitoringModel.placementDate}"
					onfocus="(type='date')" onblur="(type='text')" required />
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #adccff">
				<h3 class="panel-title">Placement At</h3>
			</div>
			<div class="panel-body">
				<input type="text" id="editPlacementAt" class="form-control"
					name="editPlacementAt" placeholder="${monitoringModel.placementAt}"
					required />
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" style="background-color: #adccff">
				<h3 class="panel-title">Notes</h3>
			</div>
			<div class="panel-body">
				<textarea rows="5" cols="25" name="editNotes" class="form-control"
					placeholder="${monitoringModel.notes}"></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;" onclick="validasi();">Save</button>

	</form>
</div>

<script type="text/javascript">
	function validasi() {
		var placementDate = document.getElementById("editPlacementDate");
		if (placementDate.value < idleDate.value) {
			placementDate.style.borderColor = "red";
		} else {
			placementDate.style.borderColor = "gray";
		}
	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>