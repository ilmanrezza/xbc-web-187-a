<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">INPUT IDLE</h1>
	</div>

	<form action="#" method="get" id="form-monitoring-add">
		<div class="form-group">
			<div class="col-md-12">
				<select id="idBiodata" name="idBiodata" class="form-control"
					required>
					<option value="">-Choose Biodata Name-</option>
					<c:forEach items="${biodataModelList}" var="biodataModel"
						varStatus="number">
						<option value="${biodataModel.id}">${biodataModel.name}</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="date" id="idleDate" name="idleDate"
					class="form-control" placeholder="Idle Date" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="lastProject" name="lastProject"
					class="form-control" placeholder="Last Project at" size="25"
					required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="idleReason" name="idleReason"
					class="form-control" placeholder="Idle Reason" size="25" required />
			</div>
		</div>

		<br />
		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;" onclick="validasi();">Save</button>

	</form>
</div>

<script type="text/javascript">
	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>