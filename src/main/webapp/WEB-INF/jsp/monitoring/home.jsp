
<div class="panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<h1>IDLE MONITORING</h1>

	<div class="box box-primary box-body">
		<table class="table" id="tbl-monitoring">
			<thead>
				<tr>
					<td><form action="#" method="get" id="form-monitoring-search">
							<input type="text" id="search" name="search" class="col-xs-6"
								placeholder="search by name" />
							<button style="background-color: white;" type="submit">O</button>
						</form></td>
					<td></td>
					<td></td>
					<td>
						<button type="button"
							class="btn btn-primary glyphicon glyphicon-plus btn-sm"
							id="btn-add"></button>
					</td>
				</tr>
			</thead>
		</table>
		<table class="table table-striped">
			<thead>
				<tr style="font-weight: bold;">
					<td>Nama</td>
					<td>Idle Date</td>
					<td>Placement Date</td>
					<td>#</td>
				</tr>
			</thead>

			<tbody id="list-monitoring">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog" style="width: 500px">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListMonitoring();

	function loadListMonitoring() {
		$.ajax({
			url : 'monitoring/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-monitoring').html(result);
			}
		});
	}

	$(document)
			.ready(
					function() {

						// ------------------------------------------------------------ //
						//fungsi ajax untuk popup input idle
						$('#btn-add').on(
								'click',
								function() {
									$
											.ajax({
												url : 'monitoring/add.html',
												type : 'get',
												dataType : 'html',
												success : function(result) {
													$('#modal-input').find(
															'.modal-body')
															.html(result);
													$('#modal-input').modal(
															'show');
												}
											});
								});
						//akhir fungsi ajax untuk popup input idle

						//fungsi ajax untuk create input idle
						$('#modal-input').on('submit', '#form-monitoring-add',
								function() {
									$.ajax({
										url : 'monitoring/create.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {

											$('#modal-input').modal('hide');
											alert("Data has been added!");
											loadListMonitoring();
										}
									});
									return false;
								});
						//akhir fungsi ajax untuk create input idle

						//fungsi ajax untuk cancel create
						$('#modal-input').on('reset', '#form-monitoring-add',
								function() {

									$.ajax({
										url : 'monitoring/reset.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {
											$('#modal-input').modal('hide');
										}
									});
									return false;
								});
						//akhir fungsi ajax untuk cancel create
						// ------------------------------------------------------------ //

						// ------------------------------------------------------------ //
						//fungsi ajax untuk popup input placement
						$('#list-monitoring')
								.on(
										'click',
										'#btn-placement',
										function() {
											var idMonitoring = $(this).val();
											$
													.ajax({
														url : 'monitoring/placement.html',
														type : 'get',
														dataType : 'html',
														data : {
															idMonitoring : idMonitoring
														},
														success : function(
																result) {
															$('#modal-input')
																	.find(
																			'.modal-body')
																	.html(
																			result);
															$('#modal-input')
																	.modal(
																			'show');
														}
													});
										});
						//akhir fungsi ajax untuk popup input placement

						//fungsi ajax untuk update input placement
						$('#modal-input')
								.on(
										'submit',
										'#form-monitoring-placement',
										function() {

											$
													.ajax({
														url : 'monitoring/updatePlacement.json',
														type : 'get',
														dataType : 'json',
														data : $(this)
																.serialize(),
														success : function(
																result) {
															if (result.kondisi == 1) {
																$(
																		'#modal-input')
																		.modal(
																				'hide');
																alert("Placement has been added!");
															} else {
																alert("Placement date must after idle date!");
															}
															loadListMonitoring();
														}
													});
											return false;
										});
						//akhir fungsi ajax untuk update input placement

						//fungsi ajax untuk cancel input placement
						$('#modal-input').on('reset',
								'#form-monitoring-placement', function() {

									$.ajax({
										url : 'monitoring/reset.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {
											$('#modal-input').modal('hide');
										}
									});
									return false;
								});
						//akhir fungsi ajax untuk cancel input placement
						// ------------------------------------------------------------ //

						// ------------------------------------------------------------ //
						//fungsi ajax untuk popup edit
						$('#list-monitoring').on(
								'click',
								'#btn-edit',
								function() {
									var idMonitoring = $(this).val();
									$
											.ajax({
												url : 'monitoring/edit.html',
												type : 'get',
												dataType : 'html',
												data : {
													idMonitoring : idMonitoring
												},
												success : function(result) {
													$('#modal-input').find(
															'.modal-body')
															.html(result);
													$('#modal-input').modal(
															'show');
												}
											});
								});
						//akhir fungsi ajax untuk popup edit

						//fungsi ajax untuk update
						$('#modal-input')
								.on(
										'submit',
										'#form-monitoring-edit',
										function() {

											$
													.ajax({
														url : 'monitoring/update.json',
														type : 'get',
														dataType : 'json',
														data : $(this)
																.serialize(),
														success : function(
																result) {
															if (result.kondisi == 1) {
																$(
																		'#modal-input')
																		.modal(
																				'hide');
																alert("Data has been updated!");
															} else {
																alert("Placement date must after idle date!");
															}
															loadListMonitoring();
														}
													});
											return false;
										});
						//akhir fungsi ajax untuk update

						//fungsi ajax untuk cancel update
						$('#modal-input').on('reset', '#form-monitoring-edit',
								function() {

									$.ajax({
										url : 'monitoring/reset.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {
											$('#modal-input').modal('hide');
										}
									});
									return false;
								});
						//akhir fungsi ajax untuk cancel update
						// ------------------------------------------------------------ //

						// ------------------------------------------------------------ //
						//fungsi ajax untuk popup remove
						$('#list-monitoring').on(
								'click',
								'#btn-remove',
								function() {
									var idMonitoring = $(this).val();
									$
											.ajax({
												url : 'monitoring/remove.html',
												type : 'get',
												dataType : 'html',
												data : {
													idMonitoring : idMonitoring
												},
												success : function(result) {
													$('#modal-input').find(
															'.modal-body')
															.html(result);
													$('#modal-input').modal(
															'show');
												}
											});
								});
						//akhir fungsi ajax untuk popup remove

						//fungsi ajax untuk update isDelete
						$('#modal-input')
								.on(
										'submit',
										'#form-monitoring-remove',
										function() {

											$
													.ajax({
														url : 'monitoring/deleteTemporary.json',
														type : 'get',
														dataType : 'json',
														data : $(this)
																.serialize(),
														success : function(
																result) {
															$('#modal-input')
																	.modal(
																			'hide');
															alert("Data has been erased!");
															loadListMonitoring();
														}
													});
											return false;
										});
						//akhir fungsi ajax untuk update isDelete

						//fungsi ajax untuk cancel delete
						$('#modal-input').on('reset',
								'#form-monitoring-remove', function() {

									$.ajax({
										url : 'monitoring/reset.json',
										type : 'get',
										dataType : 'json',
										data : $(this).serialize(),
										success : function(result) {
											$('#modal-input').modal('hide');
										}
									});
									return false;
								});
						//akhir fungsi ajax untuk cancel delete
						// ------------------------------------------------------------ //

						// ------------------------------------------------------------ //
						//fungsi ajax untuk search nama
						$('#form-monitoring-search').on('submit', function() {
							$.ajax({
								url : 'monitoring/search.html',
								type : 'get',
								dataType : 'html',
								data : $(this).serialize(),
								success : function(result) {
									alert("Data by name has been found!");
									$('#list-monitoring').html(result);
								}
							});
							return false;
						});
						//akhir fungsi ajax untuk search nama

					});
	//akhir dokumen ready
</script>