
<div class="box-header with-border panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<h1>OFFICE</h1>

	<div class="box box-primary box-body">
		<table class="table" id="tbl-office">
			<thead>
				<tr>
					<td><form action="#" method="get" id="form-office-search"
							style="margin: auto; max-width: 700px">
							<input type="text" class="col-xs-6" id="searchNama"
								name="searchNama" placeholder="search by name" />
							<button style="background-color: white;" type="submit">O</button>
						</form></td>
					<td></td>
					<td>
						<button type="button"
							class="btn btn-primary btn-md glyphicon glyphicon-plus btn-sm"
							id="btn-addOffice"></button>
					</td>
					<td></td>
				</tr>
			</thead>
		</table>
		<table class="table table-striped">
			<thead>
				<tr style="font-weight: bold;">
					<th>Name</th>
					<th>Contact</th>
					<th>#</th>
					<th></th>
				</tr>
			</thead>

			<tbody id="list-office">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog" style="width: 700px">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListOffice();

	function loadListOffice() {
		$.ajax({
			url : 'office/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-office').html(result);
			}
		});
	}

	$(document).ready(function() {

		//fungsi ajax untuk popup add
		$('#btn-addOffice').on('click', function() {
			$.ajax({
				url : 'office/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup add

		//fungsi ajax untuk create
		$('#modal-input').on('submit', '#form-office-add', function() {
			$.ajax({
				url : 'office/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("Data has been added!");
					location.reload()
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create

		//fungsi ajax untuk cancel create
		$('#modal-input').on('reset', '#form-office-add', function() {

			$.ajax({
				url : 'office/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					location.reload()
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel create

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup edit
		$('#list-office').on('click', '#btn-edit', function() {
			var idOffice = $(this).val();
			$.ajax({
				url : 'office/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					idOffice : idOffice
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-office-edit', function() {
			$.ajax({
				url : 'office/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been changed!");
					loadListOffice();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk cancel edit
		$('#modal-input').on('reset', '#form-office-edit', function() {

			$.ajax({
				url : 'office/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					location.reload()
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel edit

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup delete
		$('#list-office').on('click', '#btn-remove', function() {
			var idOffice = $(this).val();
			$.ajax({
				url : 'office/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					idOffice : idOffice
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup delete

		//fungsi ajax untuk update isDelete
		$('#modal-input').on('submit', '#form-office-remove', function() {

			$.ajax({
				url : 'office/deleteTemporary.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been erased!");
					loadListOffice();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update isDelete

		//fungsi ajax untuk cancel delete
		$('#modal-input').on('reset', '#form-office-remove', function() {

			$.ajax({
				url : 'office/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					location.reload()
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel delete

		// ------------------------------------------------------------ //

	})
</script>