<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div>
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">ADD OFFICE</h1>
	</div>

	<form action="#" method="get" id="form-office-add"
		style="margin: auto; max-width: 700px">

		<div class="form-group">
			<div class="row">
				<div class="col-lg-4">
					<input type="text" id="officeName" class="form-control"
						name="officeName" placeholder="Name" size="25" />
				</div>

				<div class="col-lg-4">
					<input type="text" id="officePhone" class="form-control"
						name="officePhone" placeholder="Phone" size="25" />
				</div>

				<div class="col-lg-4">
					<input type="text" id="email" class="form-control" name="email"
						placeholder="Email" size="25" />
				</div>

			</div>
		</div>

		<div class="form-group">
			<div class="row">

				<div class="col-lg-6">
					<textarea rows="3" cols="20" id="address" class="form-control"
						name="address" placeholder="Address"></textarea>
				</div>

				<div class="col-lg-6">
					<textarea rows="3" cols="20" id="description" class="form-control"
						name="description" placeholder="Description..."></textarea>
				</div>

			</div>
		</div>

		<hr>

		<button type="button" class="btn btn-primary btn-md"
			style="float: right; width: 100px;" id="btn-addRoom">+Room</button>

		<br>

		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Code</th>
						<th>Name</th>
						<th>Capacity</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody id="list-room">

				</tbody>
			</table>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;" onclick="validasi();">Save</button>

	</form>
</div>

<div id="modal-inputRoom" class="modal fade">
	<div class="modal-dialog" style="width: 350px">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListRoom();

	function loadListRoom() {
		$.ajax({
			url : 'room/listRoom.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-room').html(result);
			}
		});
	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})

	$(document).ready(function() {

		//fungsi ajax untuk popup add room
		$('#btn-addRoom').on('click', function() {
			$.ajax({
				url : 'room/addRoom.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-inputRoom').find('.modal-body').html(result);
					$('#modal-inputRoom').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup add room

		//fungsi ajax untuk cancel create room
		$('#modal-inputRoom').on('reset', '#form-room-add', function() {

			$.ajax({
				url : 'room/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-inputRoom').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel create room

		//fungsi ajax untuk popup edit room
		$('#list-room').on('click', '#btn-editRoom', function() {
			var idRoom = $(this).val();
			$.ajax({
				url : 'room/editRoom.html',
				type : 'get',
				dataType : 'html',
				data : {
					idRoom : idRoom
				},
				success : function(result) {
					$('#modal-inputRoom').find('.modal-body').html(result);
					$('#modal-inputRoom').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit room

		//fungsi ajax untuk cancel edit room
		$('#modal-inputRoom').on('reset', '#form-room-edit', function() {

			$.ajax({
				url : 'room/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-inputRoom').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel edit room

		//fungsi ajax untuk popup delete room
		$('#list-room').on('click', '#btn-removeRoom', function() {
			var idRoom = $(this).val();
			$.ajax({
				url : 'room/removeRoom.html',
				type : 'get',
				dataType : 'html',
				data : {
					idRoom : idRoom
				},
				success : function(result) {
					$('#modal-inputRoom').find('.modal-body').html(result);
					$('#modal-inputRoom').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup delete room

		//fungsi ajax untuk cancel delete room
		$('#modal-input').on('reset', '#form-room-remove', function() {

			$.ajax({
				url : 'room/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-inputRoom').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel delete room

	})
</script>