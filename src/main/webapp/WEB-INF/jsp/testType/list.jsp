<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${testTypeModelList}" var="testTypeModel"
	varStatus="number">
	<tr>
		<td>${testTypeModel.namaTestType}</td>
		<td>${testTypeModel.createdBy.username}</td>
		<td>
			<div class="dropdown">
				<button type="button"
					class="btn btn-primary dropdown-toggle fa fa-bars"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanden="false"></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<button style="background-color: white; border: 0px;"
						value="${testTypeModel.idTestType}" id="btn-edit">Edit</button>
					<br />
					<button style="background-color: white; border: 0px;"
						value="${testTypeModel.idTestType}" id="btn-remove">Delete</button>
				</div>
			</div>
		</td>
	</tr>
</c:forEach>