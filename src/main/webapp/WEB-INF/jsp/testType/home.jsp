
<div class="box-header with-border panel"
	style="background: white; margin-top: 50px; min-height: 550px">
	<h1>TEST TYPE</h1>

	<div class="box box-primary box-body">
		<table class="table" id="tbl-testType">
			<thead>
				<tr>
					<td><form action="#" method="get" id="form-testType-search"
							style="margin: auto; max-width: 700px">
							<input type="text" class="col-xs-6" id="searchNama"
								name="searchNama" placeholder="search by name" />
							<button style="background-color: white;" type="submit">O</button>
						</form></td>
					<td></td>
					<td>
						<button type="button"
							class="btn btn-primary btn-md glyphicon glyphicon-plus btn-sm"
							id="btn-add"></button>
					</td>
					<td></td>
				</tr>
			</thead>
		</table>
		<table class="table table-striped">
			<thead>
				<tr style="font-weight: bold;">
					<td>Name</td>
					<td>Created By</td>
					<td>#</td>
					<td></td>
				</tr>
			</thead>

			<tbody id="list-testType">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog" style="width: 350px">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListTestType();

	function loadListTestType() {
		$.ajax({
			url : 'testType/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-testType').html(result);
			}
		});
	}

	$(document).ready(function() {

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup add
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'testType/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup add

		//fungsi ajax untuk create
		$('#modal-input').on('submit', '#form-testType-add', function() {
			$.ajax({
				url : 'testType/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("Data has been added!");
					loadListTestType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create

		//fungsi ajax untuk cancel create
		$('#modal-input').on('reset', '#form-testType-add', function() {

			$.ajax({
				url : 'testType/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel create

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup edit
		$('#list-testType').on('click', '#btn-edit', function() {
			var idTestType = $(this).val();
			$.ajax({
				url : 'testType/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					idTestType : idTestType
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-testType-edit', function() {
			$.ajax({
				url : 'testType/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been changed!");
					loadListTestType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk cancel update
		$('#modal-input').on('reset', '#form-testType-edit', function() {

			$.ajax({
				url : 'testType/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel update

		// ------------------------------------------------------------ //

		//fungsi ajax untuk popup remove
		$('#list-testType').on('click', '#btn-remove', function() {
			var idTestType = $(this).val();
			$.ajax({
				url : 'testType/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					idTestType : idTestType
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup remove

		//fungsi ajax untuk update isDelete
		$('#modal-input').on('submit', '#form-testType-remove', function() {

			$.ajax({
				url : 'testType/deleteTemporary.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("Data has been erased!");
					loadListTestType();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update isDelete

		//fungsi ajax untuk cancel delete
		$('#modal-input').on('reset', '#form-testType-remove', function() {

			$.ajax({
				url : 'testType/reset.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk cancel delete

		// ------------------------------------------------------------ //

		//fungsi ajax untuk search nama
		$('#form-testType-search').on('submit', function() {
			$.ajax({
				url : 'testType/search.html',
				type : 'get',
				dataType : 'html',
				data : $(this).serialize(),
				success : function(result) {
					alert("Data by name has been found!");
					$('#list-testType').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search nama

	});
	//akhir document ready
</script>