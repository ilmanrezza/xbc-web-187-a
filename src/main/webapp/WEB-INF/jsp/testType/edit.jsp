<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<div class="panel" style="background-color: #577fbf">
		<h1 align="center" style="color: white">EDIT TEST TYPE</h1>
	</div>

	<form action="#" method="get" id="form-testType-edit">
		<input type="hidden" value="${testTypeModel.idTestType}"
			class="form-control" name="idTestType" />

		<div class="form-group">
			<div class="col-md-12">
				<input type="text" id="testTypeName" name="testTypeName"
					class="form-control" placeholder="${testTypeModel.namaTestType}"
					size="25" required />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<textarea rows="3" cols="27" id="notes" name="notes"
					class="form-control" placeholder="${testTypeModel.noteTestType}"></textarea>
			</div>
		</div>

		<button type="reset" class="btn btn-danger btn-md"
			style="width: 100px;">Cancel</button>
		<button type="submit" class="btn btn-success btn-md"
			style="float: right; width: 100px;" onclick="validasi();">Save</button>

	</form>
</div>


<script type="text/javascript">
	function validasi() {
		var lastProject = document.getElementById("testTypeName");
		if (lastProject.value == "") {
			lastProject.style.borderColor = "red";
		} else {
			lastProject.style.borderColor = "gray";
		}
	}

	$('#modal-input').modal({
		backdrop : 'static',
		keyboard : false
	})
</script>